//
//  DownloadManager.m
//  MyFirstProject
//
//  Created by Николай Голдин on 28.01.13.
//  Copyright (c) 2013 Николай Голдин. All rights reserved.
//

#import "DownloadManager.h"

@interface DownloadManager()

//FileDownloadRequestDelegate
- (void)FileDownloadRequestSuccess:(FileDownloadRequest*)request;
- (void)FileDownloadRequestFailed:(FileDownloadRequest*)request error:(NSError*)_error;

//ImageDownloadRequestDelegate
- (void)ImageDownloadRequestSuccess:(ImageDownloadRequest *)request;
- (void)ImageDownloadRequestFailed:(ImageDownloadRequest *)request error:(NSError *)error;

@end

@implementation DownloadManager

static DownloadManager* _sharedManager = nil;
+(DownloadManager*)sharedDownloadManager{
    
    @synchronized(self){
        if (_sharedManager == nil)
            _sharedManager = [[DownloadManager alloc] init];
    }
    return _sharedManager;
}

-(void) downloadFile:(NSString*)urlPath{
    FileDownloadRequest* request = [[FileDownloadRequest alloc] init];
    request.path = FILE_FOR_SAVE_FILE_STREAM;
    request.url = urlPath;
    request.delegate = self;
    
    [request start];
}

-(void)downloadImage:(NSString *)urlPath{
    ImageDownloadRequest* request = [[ImageDownloadRequest alloc] init];
    request.path = FILE_FOR_SAVE_IMAGE_STREAM;
    request.url = urlPath;
    request.delegate = self;
    
    [request start];
    
}

#pragma mark - FileDownloadRequestDelegate

-(void)FileDownloadRequestSuccess:(FileDownloadRequest *)request{
    NSLog(@"success download file");
}

-(void)FileDownloadRequestFailed:(FileDownloadRequest *)request error:(NSError *)_error{
    NSLog(@"failer download file. Error -> %@", _error);
}

#pragma mark - ImageDownloadRequestDelegate

-(void)ImageDownloadRequestSuccess:(ImageDownloadRequest *)request{
    NSLog(@"success download image");
}

-(void)ImageDownloadRequestFailed:(ImageDownloadRequest *)request error:(NSError *)error{
    NSLog(@"failer downloaf image. Error -> %@", error);
}

@end
