//
//  ImageDownloadRequest.h
//  MyFirstProject
//
//  Created by Николай Голдин on 28.01.13.
//  Copyright (c) 2013 Николай Голдин. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFImageRequestOperation.h"

@class ImageDownloadRequest;
@protocol ImageDownloadRequestDelegate <NSObject>
@optional
- (void) ImageDownloadRequestSuccess:(ImageDownloadRequest*)request;
- (void) ImageDownloadRequestFailed:(ImageDownloadRequest*)request error:(NSError*)error;
@end

@interface ImageDownloadRequest : NSObject{
    NSString* path;
    NSString* url;
    UIImage* image;
    
    id<ImageDownloadRequestDelegate> delegate;
}

@property (retain, nonatomic) NSString* path;
@property (retain, nonatomic) NSString* url;
@property (retain, nonatomic, readonly) UIImage* image;
@property (assign, nonatomic) id<ImageDownloadRequestDelegate> delegate;

-(void) start;
-(void) cancel;

@end
