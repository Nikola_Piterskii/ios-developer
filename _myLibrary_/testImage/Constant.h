//
//  Constant.h
//  MyFirstProject
//
//  Created by Николай Голдин on 25.01.13.
//  Copyright (c) 2013 Николай Голдин. All rights reserved.
//

#define SHOW_ABOUT_NOTIFICATION @"SHOW_ABOUT_NOTIFICATION"
#define CLOSE_ABOUT_NOTIFICATION @"CLOSE_ABOUT_NOTIFICATION"

#define FILE_FOR_SAVE_FILE_STREAM @"/Users/ngoldin/Desktop/testStream/first.png"
#define FILE_FOR_SAVE_IMAGE_STREAM @"/Users/ngoldin/Desktop/testImage/first.jpg"
