//
//  FileRequest.m
//  MyFirstProject
//
//  Created by Николай Голдин on 28.01.13.
//  Copyright (c) 2013 Николай Голдин. All rights reserved.
//

#import "FileDownloadRequest.h"
#import "AFHTTPRequestOperation.h"

@interface FileDownloadRequest(){
    AFHTTPRequestOperation* request;
}
@property (retain, nonatomic) AFHTTPRequestOperation* request;
@end

@implementation FileDownloadRequest

@synthesize request;
@synthesize url;
@synthesize path;
@synthesize timeout;
@synthesize delegate;

-(id)init{
    self = [super init];
    if (self){
        
        self.timeout = 5;
        //url = @"http://appapix4.neoline.biz/apple-neobooks/books/2e108bb5e175faf3/cover_name_library~iphone.png";
        //path = @"/Users/ngoldin/Desktop/testStream/testImage";
        
        //self.timeout = 5;
        //url = @"http://appapi.neoline.biz/apple-neobooks/getCover?book=6057&test=0&type=library&displayWidth=320&displayHeight=480";
        //url = @"http://appapix4.neoline.biz/apple-neobooks/books/2e108bb5e175faf3/cover_name_library~iphone.png";
        //url = @"/Users/ngoldin/Desktop/icon_lang_rus.png";
        //path = @"/Users/ngoldin/Desktop/testStream/testImage.png";
    }
    return self;
}

-(void) start{
    NSMutableURLRequest *urlRequest = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:url] cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:self.timeout];
    self.request = [[[AFHTTPRequestOperation alloc] initWithRequest:urlRequest] autorelease];
    
    NSOutputStream *stream = [NSOutputStream outputStreamToFileAtPath:path append:NO];
    [self.request setOutputStream:stream];
    
    [self.request setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject){
        
        [operation.outputStream close];
        if ([self.delegate respondsToSelector:@selector(FileDownloadRequestSuccess:)])
            [self.delegate FileDownloadRequestSuccess:self];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error){
        
        if ([self.delegate respondsToSelector:@selector(FileDownloadRequestFailed:error:)])
            [self.delegate FileDownloadRequestFailed:self error:error];
    }];
        
    [self.request start];
}

-(void) cancel{
    [self.request cancel];
    [self.request setDownloadProgressBlock:nil];
    self.request = nil;
}

- (void)dealloc
{
    [self cancel];
    [self.request release];
    [self.path release];
    [self.url release];
    [super dealloc];
}

@end
