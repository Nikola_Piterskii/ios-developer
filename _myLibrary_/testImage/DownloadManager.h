//
//  DownloadManager.h
//  MyFirstProject
//
//  Created by Николай Голдин on 28.01.13.
//  Copyright (c) 2013 Николай Голдин. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FileDownloadRequest.h"
#import "ImageDownloadRequest.h"

@interface DownloadManager : NSObject<FileDownloadRequestDelegate, ImageDownloadRequestDelegate>

+(DownloadManager*)sharedDownloadManager;

-(void) downloadFile:(NSString*)urlPath;
-(void) downloadImage:(NSString*)urlPath;

@end
