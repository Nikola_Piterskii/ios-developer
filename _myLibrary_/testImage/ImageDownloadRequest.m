//
//  ImageDownloadRequest.m
//  MyFirstProject
//
//  Created by Николай Голдин on 28.01.13.
//  Copyright (c) 2013 Николай Голдин. All rights reserved.
//

#import "ImageDownloadRequest.h"

@interface ImageDownloadRequest(){
    AFImageRequestOperation* request;
}

@property (retain, nonatomic) AFImageRequestOperation* request;
@property (retain, nonatomic, readwrite) UIImage* image;

-(void) saveImage;
@end

@implementation ImageDownloadRequest

@synthesize path;
@synthesize url;
@synthesize image;
@synthesize request;
@synthesize delegate;

-(void)start{
    NSMutableURLRequest* urlRequest = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:self.url] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:30];
    
    self.request = [[[AFImageRequestOperation alloc] initWithRequest:urlRequest] autorelease];
    
    [self.request setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject){
        
        self.image = responseObject;
        if (self.image)
            [self saveImage];
        
        if ([self.delegate respondsToSelector:@selector(ImageDownloadRequestSuccess:)])
            [self.delegate ImageDownloadRequestSuccess:self];
    }failure:^(AFHTTPRequestOperation *operation, NSError *error){
        
        if ([self.delegate respondsToSelector:@selector(ImageDownloadRequestFailed:error:)])
            [self.delegate ImageDownloadRequestFailed:self error:error];
    }];
    
    [self.request start];
}

-(void)cancel{
    [self.request cancel];
}

-(void)saveImage{
    NSData* data = UIImagePNGRepresentation(self.image);
    if (!data)
        return;
    
    //NSString* storePath = FILE_FOR_SAVE_IMAGE_STREAM;
    
    NSError* error = nil;
    [data writeToFile:self.path options:NSDataWritingFileProtectionNone error:&error];
    if (error)
    {
        NSLog(@"[ImageDownloadRequest saveImage] error: %@", error);
    }
}

- (void)dealloc
{
    [self cancel];
    [self.request release];
    [self.path release];
    [self.url release];
    [self.image release];
    [super dealloc];
}
@end
