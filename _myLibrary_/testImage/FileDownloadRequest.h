//
//  FileRequest.h
//  MyFirstProject
//
//  Created by Николай Голдин on 28.01.13.
//  Copyright (c) 2013 Николай Голдин. All rights reserved.
//

#import <UIKit/UIKit.h>

@class FileDownloadRequest;
@protocol FileDownloadRequestDelegate <NSObject>
@optional
- (void)FileDownloadRequestSuccess:(FileDownloadRequest*)request;
- (void)FileDownloadRequestFailed:(FileDownloadRequest*)request error:(NSError*)_error;
@end

@interface FileDownloadRequest : NSObject{
    NSString* path;
    NSString* url;
    NSTimeInterval timeout;
    id<FileDownloadRequestDelegate> delegate;
}

@property (retain, nonatomic) NSString* path;
@property (retain, nonatomic) NSString* url;
@property (assign, nonatomic) NSTimeInterval timeout;
@property (assign, nonatomic) id<FileDownloadRequestDelegate> delegate;

-(void) start;
-(void) cancel;

@end
