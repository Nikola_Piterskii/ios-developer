//
//  HomeScreeenViewController.h
//  _myLibrary_
//
//  Created by Алексей Иванков on 1/24/13.
//  Copyright (c) 2013 Алексей Иванков. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "iCarousel.h"
#import "BaseCarouselCell.h"
#import "OpenViewDelegate.h"
#import "JsonObjectDelegate.h"
#import "DownloadManagerObject.h"


@interface HomeScreeenViewController : BaseViewController<iCarouselDataSource, iCarouselDelegate,OpenViewDelegate, UIAlertViewDelegate,JsonObjectDelegate,ImageDownloadRequestDelegate>
{
    iCarousel *_carousel;
    BaseCarouselCell *_carouselCell;
}
@property (retain,nonatomic) IBOutlet iCarousel *carousel;
@property (retain, nonatomic) IBOutlet BaseCarouselCell *carouselCell;
@property (retain, nonatomic) NSFetchedResultsController* fetchedResultsController;


- (IBAction)ontouchInformation:(id)sender;
- (IBAction)onTouchCatalog:(id)sender;

@end
