//
//  CatalogViewController.m
//  _myLibrary_
//
//  Created by Алексей Иванков on 1/24/13.
//  Copyright (c) 2013 Алексей Иванков. All rights reserved.
//

#import "CategoryViewController.h"
#import "Constants.h"
#import "BaseViewController.h"
#import "BaseCatalogCell.h"
#import "CategoryObject.h"
#import "Utils.h"
#import "DataStorage.h"

@interface CategoryViewController ()

@end

@implementation CategoryViewController
@synthesize tableCatalog;
@synthesize fetchedResultController;

-(void)dealloc
{
    [tableCatalog release];
    [fetchedResultController release];
    [super dealloc];
}

-(NSFetchedResultsController*)fetchedResultController{
    if (fetchedResultController == nil){
        fetchedResultController = [[[DataStorage sharedDataStorage] genreFetchedResultsController] retain];
        fetchedResultController.delegate = self;
    }
    return fetchedResultController;
}

#pragma mark - tableView DataSource

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[[self.fetchedResultController sections] objectAtIndex:section] numberOfObjects];
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    BaseCatalogCell* cell;
    static NSString* templateName = @"Template_Cell";
    cell = (BaseCatalogCell*)[self.tableCatalog dequeueReusableCellWithIdentifier:templateName];
    
    NSString* nibName = nil;
    
    if (cell == nil){
        if ([Utils isIpad]){
            nibName = @"CatalogCell_ipad";
        }else{
            nibName = @"CatalogCell_iphone";
        }
        
        cell = [[[NSBundle mainBundle] loadNibNamed:nibName owner:self options:nil] objectAtIndex:0];
    }
    
    Genre* genre = [self.fetchedResultController objectAtIndexPath:indexPath];
    
    cell.nameCatalog.text = genre.genreName;
    
    return cell;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [[self.fetchedResultController sections] count];
}

#pragma mark - delegate methods tableView

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [[NSNotificationCenter defaultCenter] postNotificationName:REFRESH_THE_CAROUSEL object:nil];
    
    Genre* genre = [self.fetchedResultController objectAtIndexPath:indexPath];
    
    NSMutableDictionary* dictionary = [NSMutableDictionary dictionary];
    [dictionary setValue:genre.genreId forKey:@"GENRE_ID"];
    [dictionary setValue:genre.genreName forKey:@"GENRE_NAME"];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:ROTATE_THE_CAROUSEL object:dictionary];
    
    [self.delegate nextViewControllerName:[BaseViewController getHomeName] animation:UIViewAnimationOptionTransitionCrossDissolve];
}

#pragma mark - CatalogViewController methods

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.view addSubview:self.tableCatalog];
}


+ (NSString*)typeStringForType:(CategoryEnum)_type {
    NSString *key = [NSString stringWithFormat:@"%u",_type];
    return NSLocalizedString( key , nil);
}

-(IBAction)onTouchHomeController:(id)sender
{
    [self.delegate nextViewControllerName:[BaseViewController getHomeName] animation:UIViewAnimationOptionTransitionFlipFromRight ];
    [[NSNotificationCenter defaultCenter] postNotificationName:REFRESH_THE_CAROUSEL object:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:SET_CONTENT_FRAME object:nil];
}

-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [self setFrame];
}

-(void)setFrame
{
  self.tableCatalog.frame = [self getFrame];
}

@end
