//
//  StroreKit.m
//  _myLibrary_
//
//  Created by Алексей Иванков on 2/20/13.
//  Copyright (c) 2013 Алексей Иванков. All rights reserved.
//

#import "StroreKitHandler.h"

typedef  void (^RequestProductsCompletionHandler)(BOOL success, NSArray *product);

@interface StroreKitHandler()
{

    SKProductsRequest *_productRequest; //хранит информацию о запросе продуктов в памяти
    RequestProductsCompletionHandler _completionHandler;
    NSSet *_productIdentifiers;
    NSMutableSet *_purchasedProductIdentifiriers;
    
}
-(id) initWithProductIdentifiries:(NSSet*)productIdentifiries; //принимает список идентификаторов продуктов
-(void)requestProductsWithCompletionHandler:(RequestProductsCompletionHandler)completionHandler; //получает информацию о продуктах

-(void)buyProduct:(SKProduct*)product;
-(BOOL)productPurchased:(NSString*)productIdentifier;

@end

@implementation StroreKitHandler

+(StroreKitHandler*)getInstance
{
    static StroreKitHandler *_storeKitHandler = nil;
    if(_storeKitHandler ==nil)
    {
        _storeKitHandler = [[StroreKitHandler alloc]init];
    }
    return  _storeKitHandler;
}

-(void)requestProductsWithCompletionHandler:(RequestProductsCompletionHandler)completionHandler
{
    _completionHandler = [completionHandler copy]; //делаем копию для асинхронного получения результата
    _productRequest = [[SKProductsRequest alloc]initWithProductIdentifiers:_productIdentifiers]; //отвечает за выборку информации о продуктах с iTunes connect
    _productRequest.delegate = self;
    [_productRequest start];
}

-(void) paymentQueue:(SKPaymentQueue*)queue updatedTransactions:(NSArray*)transactions
{
    for (SKPaymentTransaction* transaction in transactions)
    {
        if (transaction.transactionState == SKPaymentTransactionStatePurchased)
        {
            DLog(@"StoreKit transaction purchased: %@", transaction.payment.productIdentifier);
            //[self recordTransaction:transaction withRestoredFlag:NO];
        }
        else if (transaction.transactionState == SKPaymentTransactionStateFailed)
        {
            DLog(@"StoreKit transaction failed: %@", transaction.payment.productIdentifier);
          //  [self failedTransaction:transaction];
        }
        else if (transaction.transactionState == SKPaymentTransactionStateRestored)
        {
            DLog(@"StoreKit transaction restored: %@", transaction.payment.productIdentifier);
           // [self recordTransaction:transaction withRestoredFlag:YES];
        }
    }
}


-(void)requestProduct
{

}

-(void)paymentQueue:(SKPaymentQueue *)queue removedTransactions:(NSArray *)transactions
{
    DLog(@"removedTransactions");
}

-(void)paymentQueue:(SKPaymentQueue *)queue restoreCompletedTransactionsFailedWithError:(NSError *)error
{
   DLog(@"restoreCompletedTransactionsFailedWithError");
}

-(void)paymentQueue:(SKPaymentQueue *)queue updatedDownloads:(NSArray *)downloads
{
   DLog(@"updatedDownloads");
}

-(void)paymentQueueRestoreCompletedTransactionsFinished:(SKPaymentQueue *)queue
{
   DLog(@"paymentQueueRestoreCompletedTransactionsFinished");
}

-(void)buyProduct:(SKProduct *)product //если возможна то добавляем покупку в очередь
{
    SKPayment *payment = [SKPayment paymentWithProduct:product];
    [[SKPaymentQueue defaultQueue] addPayment:payment];
}

-(BOOL)productPurchased:(NSString *)productIdentifier //проверка возможности покупки
{
    return [_purchasedProductIdentifiriers containsObject:productIdentifier];
}

#pragma mark - SKProductRequestDelegate

-(void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response //вызывается при получении списка продуктов
{
    _productRequest = nil;
    NSArray *skProduct = response.products;
    for (SKProduct *product in skProduct) {
        NSLog(@"Found product: %@ %@ %0.2f",
              product.productIdentifier,
              product.localizedTitle,
              product.price.floatValue);
    }
    _completionHandler(YES,skProduct); //передаем результат в блок
    _completionHandler = nil;
}

-(void)request:(SKRequest *)request didFailWithError:(NSError *)error // при ошибке запроса
{
    _productRequest = nil;
    _completionHandler(NO,nil);
    _completionHandler = nil;
}


@end
