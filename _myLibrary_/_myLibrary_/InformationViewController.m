//
//  InformationViewController.m
//  _myLibrary_
//
//  Created by Алексей Иванков on 1/24/13.
//  Copyright (c) 2013 Алексей Иванков. All rights reserved.
//

#import "InformationViewController.h"
#import "Constants.h"
#import "BaseViewController.h"

@interface InformationViewController ()

@end

@implementation InformationViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(IBAction)onTouchHomeController:(id)sender
{
  [self.delegate nextViewControllerName:[BaseViewController getHomeName] animation:UIViewAnimationOptionTransitionCurlUp ];
  [[NSNotificationCenter defaultCenter] postNotificationName:REFRESH_THE_CAROUSEL object:nil];
}
@end
