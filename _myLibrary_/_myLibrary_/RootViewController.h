//
//  RootViewController.h
//  _myLibrary_
//
//  Created by Алексей Иванков on 1/24/13.
//  Copyright (c) 2013 Алексей Иванков. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SplashViewController.h"
#import "HomeScreeenViewController.h"
#import "InformationViewController.h"
#import "CategoryViewController.h"
#import "BaseViewController.h"
#import "NavigationStack.h"
#import "ChangeViewControllerDelegate.h"
#import "SelectViewController.h"
#import "NavigationBar_shared.h"


@interface RootViewController : UIViewController <ChangeViewControllerDelegate>
{
    SplashViewController *_splashViewController;
    HomeScreeenViewController *_homeScreenViewController;
    InformationViewController *_informationViewController;
    CategoryViewController *_catalogViewController;
    SelectViewController*_selectViewController;
}
@property (retain,nonatomic) SplashViewController *splashViewController;
@property (retain,nonatomic) HomeScreeenViewController *homeScreenViewController;
@property (retain,nonatomic) InformationViewController *informationViewController;
@property (retain,nonatomic) CategoryViewController *catalogViewController;
@property (retain,nonatomic) BaseViewController *curentViewController;
@property (retain, nonatomic) BaseViewController *baseViewController;
@property (retain,nonatomic) SelectViewController *selectViewController;
@property (retain,nonatomic) NavigationStack *navigationStack;
@property (retain,nonatomic ) NavigationBar_shared *navigationBar;
@property (retain, nonatomic) UIApplication *myApplication;

@end
