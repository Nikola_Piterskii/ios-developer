
#import <UIKit/UIKit.h>
@class KTOneFingerRotationGestureRecognizer;
@protocol RecognizerStop
- (void)start:(KTOneFingerRotationGestureRecognizer*) recognizer;
- (void)stop:(KTOneFingerRotationGestureRecognizer*) recognizer;
@end

@interface KTOneFingerRotationGestureRecognizer : UIPanGestureRecognizer 
{
    id<RecognizerStop> delegate;
    NSDate *methodFinish;
    CGFloat speed;
    float averageSpeed;
    int count;
}

/**
 The rotation of the gesture in radians since its last change.
 */
@property (retain) id delegate;
@property (nonatomic, assign) CGFloat rotation;
@property (nonatomic, assign) CGFloat prevRotation;
@property (nonatomic, assign) CGFloat speed;
@property (nonatomic, assign) float averageSpeed;
@property (nonatomic, retain) NSDate *methodFinish;
- (void) initVal;


@end
