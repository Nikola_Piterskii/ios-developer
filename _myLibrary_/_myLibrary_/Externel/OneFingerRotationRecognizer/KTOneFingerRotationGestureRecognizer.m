
#import "KTOneFingerRotationGestureRecognizer.h"
#import <UIKit/UIGestureRecognizerSubclass.h>


@implementation KTOneFingerRotationGestureRecognizer

@synthesize rotation = rotation_;
@synthesize delegate;
@synthesize methodFinish;
@synthesize speed;
@synthesize prevRotation;
@synthesize averageSpeed;
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
   // Fail when more than 1 finger detected.
   if ([[event touchesForGestureRecognizer:self] count] > 1) {
      [self setState:UIGestureRecognizerStateFailed];
   }
}
- (void) initVal
{
    self.averageSpeed = 0.0;
    count = 0;
}
- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
   if ([self state] == UIGestureRecognizerStatePossible) {
      [self setState:UIGestureRecognizerStateBegan];
       
       if(self.delegate != nil)
       {
           if ([self.delegate respondsToSelector:@selector(start:)])
           {
               [self.delegate start:self];
           }
       }

       
   } else {
      [self setState:UIGestureRecognizerStateChanged];
   }

   // We can look at any touch object since we know we 
   // have only 1. If there were more than 1 then 
   // touchesBegan:withEvent: would have failed the recognizer.
   UITouch *touch = [touches anyObject];

    
    NSDate *start = [NSDate date];
    
     
    
    NSTimeInterval executionTime = [start timeIntervalSinceDate:self.methodFinish];
    
    //NSLog(@"Execution Time: %f", executionTime);
    self.methodFinish = start;
    
    
   // To rotate with one finger, we simulate a second finger.
   // The second figure is on the opposite side of the virtual
   // circle that represents the rotation gesture.
    
   UIView *view = [self view];
   CGPoint center = CGPointMake(CGRectGetMidX([view bounds]), CGRectGetMidY([view bounds]));
   CGPoint currentTouchPoint = [touch locationInView:view];
   CGPoint previousTouchPoint = [touch previousLocationInView:view];
   
   CGFloat angleInRadians = atan2f(currentTouchPoint.y - center.y, currentTouchPoint.x - center.x) - atan2f(previousTouchPoint.y - center.y, previousTouchPoint.x - center.x);
    
    if(angleInRadians > 1.0)
    {
        angleInRadians = self.prevRotation;
    }
    else if(angleInRadians < -1.0)
    {
        angleInRadians = self.prevRotation;
    }
    else 
    {
        self.prevRotation = angleInRadians;
    }
    //if((angleInRadians >=0 && angleInRadians <=1) || (angleInRadians <=0 && angleInRadians >= -1)) 
        {
            DLog(@"angleInRadians: %f", angleInRadians);
            CGFloat speed_ = (CGFloat) angleInRadians/(CGFloat)executionTime;
           
            
            if(speed_ > 0 && speed_ < 0.2)
                speed_ = 0.2;
            else if(speed_ > 0.2 && speed_ < 0.4)
                speed_ = 0.4;
            else if(speed_ > 0.4 && speed_ < 0.6)
                speed_ = 0.6;
            else if(speed_ > 0.6 && speed_ < 0.8)
                speed_ = 0.8;
            else if(speed_ > 0.8 && speed_ < 1.0)
                speed_ = 1.0;
            else if(speed_ > 1.0 && speed_ < 2.0)
                speed_ = 2.0;
            else if(speed_ > 2.0 && speed_ < 4.0)
                speed_ = 4.0;
            else if(speed_ > 4.0 && speed_ < 6.0)
                speed_ = 6.0;
            else if(speed_ > 6.0 && speed_ < 8.0)
                speed_ = 8.0;
            else if(speed_ > 8.0 && speed_ < 10.0)
                speed_ = 10.0;
            else if(speed_ > 10.0 && speed_ < 12.0)
                speed_ = 12.0;
            else if(speed_ > 12.0 && speed_ < 14.0)
                speed_ = 14.0;
            else if(speed_ > 14.0 && speed_ < 16.0)
                speed_ = 16.0;
            else if(speed_ > 16.0 && speed_ < 18.0)
                speed_ = 18.0;
            else if(speed_ > 18.0 && speed_ < 30.0)
                speed_ = 20.0;
            
            
            if(speed_ < 0 && speed_ > -0.2)
                speed_ = -0.2;
            else if(speed_ < -0.2 && speed_ > -0.4)
                speed_ = -0.4;
            else if(speed_ < -0.4 && speed_ > -0.6)
                speed_ = -0.6;
            else if(speed_ < -0.6 && speed_ > -0.8)
                speed_ = -0.8;
            else if(speed_ < -0.8 && speed_ > -1.0)
                speed_ = -1.0;
            else if(speed_ < -1.0 && speed_ > -2.0)
                speed_ = -2.0;
            else if(speed_ < -2.0 && speed_ > -4.0)
                speed_ = -4.0;
            else if(speed_ < -4.0 && speed_ > -6.0)
                speed_ = -6.0;
            else if(speed_ < -6.0 && speed_ > -8.0)
                speed_ = -8.0;
            else if(speed_ < -8.0 && speed_ > -10.0)
                speed_ = -10.0;
            else if(speed_ < -10.0 && speed_ > -12.0)
                speed_ = -12.0;
            else if(speed_ < -12.0 && speed_ > -14.0)
                speed_ = -14.0;
            else if(speed_ > 14.0 && speed_ < 16.0)
                speed_ = 16.0;
            else if(speed_ < -16.0 && speed_ > -18.0)
                speed_ = -18.0;
            else if(speed_ < -18.0 && speed_ > -30.0)
                speed_ = -20.0;

            
            
            
             DLog(@"Speed>>>: %f", speed_);
            [self setSpeed:speed_];
            
                        
            [self setRotation:angleInRadians];
            //NSLog(@"Speed: %f", speed_);
            
            /*
            if(speed_ > 0.0f)
                speed_ = 10.0;
            else if(speed_< 0.0) 
            {
                speed_ = -10.0;
            }
             */
            
        }
    //[self s
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    DLog(@"touchesEnded %@ %@",touches, event);
   // Perform final check to make sure a tap was not misinterpreted.
   if ([self state] == UIGestureRecognizerStateChanged) {
      [self setState:UIGestureRecognizerStateEnded];
       //self.accessibilityActivationPoint
       
       
       //UIPanGestureRecognizer *pan = (UIPanGestureRecognizer*)self.superclass;
       //[self setSpeed:0.0f];
       
       
   } else {
      [self setState:UIGestureRecognizerStateFailed];
   }
    if(self.delegate != nil)
    {
        if ([self.delegate respondsToSelector:@selector(stop:)])
        {
            DLog(@"ssstop");
            [self.delegate stop:self];
        }
    } 
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
   [self setState:UIGestureRecognizerStateFailed];
    if(self.delegate != nil)
    {
        if ([self.delegate respondsToSelector:@selector(stop:)])
        {
            DLog(@"ssstop");
            [self.delegate stop:self];
        }
    } 
}

@end
