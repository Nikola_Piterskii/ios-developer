//
//  SharedObjectsHelper.h
//  NeoFilm
//
//  Created by Андрей Анисимов on 06.07.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
@interface SharedObjectsHelper : NSObject{
    
}
@property (nonatomic, retain) NSMutableArray *buyedFilmsSharedArray;
@property (nonatomic, retain) NSMutableArray *restoredFilmsSharedArray;
@property (nonatomic, retain) NSNumber *genrePos;
@property (nonatomic, retain) NSNumber *tablePos;
@property (nonatomic, retain) NSString *genreState;
@property (nonatomic, retain) NSNumber *appOneTimesRun;
+ (id)sharedInstance;
@end
