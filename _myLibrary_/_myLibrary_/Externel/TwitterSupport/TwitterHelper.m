//
//  TwitterHelper.m
//  TwitterSupportDemo
//
//  Created by Toni Sala Echaurren on 14/12/11.
//  Copyright 2011 Toni Sala. All rights reserved.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//

#import "TwitterHelper.h"
#import "TwitterViewController.h"
#import <Twitter/Twitter.h>

@implementation TwitterHelper

#pragma mark - Private Methods

-(void) presentSendTweetSheetOnViewController:(UIViewController*)viewController withInitialText:(NSString*)initialText {
    CGFloat osVersion = [[[UIDevice currentDevice] systemVersion] floatValue];
    if (osVersion >= 5.0)
    {
        if ([TWTweetComposeViewController canSendTweet])
        {
            TWTweetComposeViewController *tweetSheet = [[TWTweetComposeViewController alloc] init];
            [tweetSheet setInitialText:initialText];
            
            [viewController presentModalViewController:tweetSheet animated:YES];
            
        } else {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Can't send tweet" 
                                                                message:@"You can't send a tweet right now. Make sure your device has an internet connection and you have at least one Twitter account setup" 
                                                               delegate:self 
                                                      cancelButtonTitle:@"OK" 
                                                      otherButtonTitles:nil];
            [alertView show];
        }
        
    } else {
        TwitterViewController *twitterView = [[TwitterViewController alloc] initWithNibName:@"TwitterViewController" bundle:nil];
        twitterView.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
        twitterView.modalPresentationStyle = UIModalPresentationFormSheet;
        twitterView.tweetSuggested = initialText;
        [viewController presentModalViewController:twitterView animated:NO];
        
        //[viewController.view addSubview:twitterView.view];
        //CGPoint cnt = viewController.view.center;
        //cnt.y = cnt.y - 100.0;
        //twitterView.view.center = cnt;
        
        [twitterView release];
    }
}

#pragma mark -
#pragma mark Singleton Variables
static TwitterHelper *singletonDelegate = nil;

#pragma mark -
#pragma mark Singleton Methods

+ (TwitterHelper *)sharedInstance {
	@synchronized(self) {
		if (singletonDelegate == nil) {
			[[self alloc] init]; // assignment not done here
		}
	}
	return singletonDelegate;
}

+ (id)allocWithZone:(NSZone *)zone {
	@synchronized(self) {
		if (singletonDelegate == nil) {
			singletonDelegate = [super allocWithZone:zone];
			// assignment and return on first allocation
			return singletonDelegate;
		}
	}
	// on subsequent allocation attempts return nil
	return nil;
}

- (id)copyWithZone:(NSZone *)zone {
	return self;
}

- (id)retain {
	return self;
}

- (unsigned)retainCount {
	return UINT_MAX;  // denotes an object that cannot be released
}

- (void)release {
	//do nothing
}

- (id)autorelease {
	return self;
}

#pragma mark - Public Methods

-(void) presentSendTweetSheetOnViewController:(UIViewController*)viewController withNewHighScore:(int)newNighScore {
    NSString *initialText = [NSString stringWithFormat:kCustomMessage, newNighScore, kAppName, kServerLink, kImageSrc];
    [self presentSendTweetSheetOnViewController:viewController withInitialText:initialText];
}

@end
