//
//  TwitterViewController.h
//
//  Created by Toni Sala Echaurren on 19/10/11.
//  Copyright 2011 Toni Sala. All rights reserved.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//

#import <UIKit/UIKit.h>
#import "SA_OAuthTwitterController.h"
#import "MBProgressHUD.h"

#define kOAuthConsumerKey       @"HYWMx9WQnqV3bRkoWSOKYw" // REPLACE HYWMx9WQnqV3bRkoWSOKYw
#define kOAuthConsumerSecret    @"NkhpcE5Y7YpupCVolXn3P8mrKJPPYZCzt7DXfbANUhA" // REPLACE

#define kAlertDelayTime 2.0
#define kTweetLenght    140

@class SA_OAuthTwitterEngine;

@interface TwitterViewController : UIViewController <SA_OAuthTwitterControllerDelegate, MBProgressHUDDelegate> {
	SA_OAuthTwitterEngine *_engine;
    MBProgressHUD *progressHUD;

    IBOutlet UINavigationBar *navigationBar;
    IBOutlet UIBarButtonItem *buttonCancel;
    IBOutlet UIBarButtonItem *buttonSend;
    IBOutlet UITextView *textView;
    IBOutlet UILabel *labelCharsCount;
    
    NSString *tweetSuggested;
}

@property (nonatomic, retain) IBOutlet UINavigationBar *navigationBar;
@property (nonatomic, retain) IBOutlet UIBarButtonItem *buttonCancel;
@property (nonatomic, retain) IBOutlet UIBarButtonItem *buttonSend;
@property (nonatomic, retain) IBOutlet UITextView *textView;
@property (nonatomic, retain) IBOutlet UILabel *labelCharsCount;

@property (nonatomic, retain) NSString *tweetSuggested;

- (IBAction)buttonCancelTouchUp:(id)sender;
- (IBAction)buttonSendTouchUp:(id)sender;

-(void)textChanged:(NSNotification*)notification;

@end

