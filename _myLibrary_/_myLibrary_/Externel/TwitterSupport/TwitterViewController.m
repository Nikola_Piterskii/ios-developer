//
//  TwitterViewController.m
//
//  Created by Toni Sala Echaurren on 19/10/11.
//  Copyright 2011 Toni Sala. All rights reserved.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//

#import "TwitterViewController.h"
#import "SA_OAuthTwitterEngine.h"

@implementation TwitterViewController

@synthesize navigationBar;
@synthesize buttonCancel;
@synthesize buttonSend;
@synthesize textView;
@synthesize labelCharsCount;

@synthesize tweetSuggested;

#pragma mark - Private Methods

-(void) delayedDismiss {
    [self dismissModalViewControllerAnimated:YES];
}

-(void) enableSend {
    labelCharsCount.textColor = [UIColor blackColor];
    buttonSend.enabled = YES;
}

-(void) disableSend {
    labelCharsCount.textColor = [UIColor redColor];
    buttonSend.enabled = NO;
}

-(void) showUI {
    navigationBar.topItem.title = [NSString stringWithFormat:@"@%@", _engine.username];
    buttonCancel.enabled = YES;
    buttonSend.enabled = YES;
    textView.hidden = NO;
    labelCharsCount.hidden = NO;
    
    int charsLeft = kTweetLenght - textView.text.length;
    if (charsLeft < 0) {
        [self disableSend];
    } else {
        [self enableSend];
    }
}

-(void) hideUI {
    navigationBar.topItem.title = NSLocalizedString(@"", nil);
    buttonCancel.enabled = NO;
    buttonSend.enabled = NO;
    textView.hidden = YES;
    labelCharsCount.hidden = YES;
}

#pragma mark - SA_OAuthTwitterEngineDelegate
- (void) storeCachedTwitterOAuthData: (NSString *) data forUsername: (NSString *) username {
    DLog(@"storing twitter login: %@", username);
    
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];

	[defaults setObject: data forKey: @"authData"];
	[defaults synchronize];
    
    DLog(@"cached login data: %@", data);
}

- (NSString *) cachedTwitterOAuthDataForUsername: (NSString *) username {
    DLog(@"twitter login: %@", username);
    
	return [[NSUserDefaults standardUserDefaults] objectForKey: @"authData"];
}

#pragma mark SA_OAuthTwitterControllerDelegate
- (void) OAuthTwitterController: (SA_OAuthTwitterController *) controller authenticatedWithUsername: (NSString *) username {
	DLog(@"Authenicated for %@", username);
    
    [self showUI];
}

- (void) OAuthTwitterControllerFailed: (SA_OAuthTwitterController *) controller {
	DLog(@"Authentication Failed!");
}

- (void) OAuthTwitterControllerCanceled: (SA_OAuthTwitterController *) controller {
	DLog(@"Authentication Canceled.");
    
    [self performSelector:@selector(delayedDismiss) withObject:nil afterDelay:0.7];
}

#pragma mark TwitterEngineDelegate
- (void) requestSucceeded: (NSString *) requestIdentifier {
	DLog(@"Request %@ succeeded", requestIdentifier);
    
    progressHUD.customView = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"check-mark.png"]] autorelease];
    progressHUD.mode = MBProgressHUDModeCustomView;
    progressHUD.labelText = NSLocalizedString(@"Operation Successful", nil);
    
    [progressHUD hide:YES afterDelay:kAlertDelayTime];
    [self performSelector:@selector(delayedDismiss) withObject:nil afterDelay:kAlertDelayTime];
}

- (void) requestFailed: (NSString *) requestIdentifier withError: (NSError *) error {
	DLog(@"Request %@ failed with error: %@", requestIdentifier, error);
    
    progressHUD.customView = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"X-mark.png"]] autorelease];
    progressHUD.mode = MBProgressHUDModeCustomView;
    progressHUD.labelText = NSLocalizedString(@"An Error Occured", nil);
    [progressHUD hide:YES afterDelay:kAlertDelayTime];
    
}

#pragma mark - View lifecycle

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
	[_engine release];
    
    [navigationBar release];
    [buttonCancel release];
    [buttonSend release];
    [textView release];
    [labelCharsCount release];
    
    [super dealloc];
}

-(void) initLabels {
    buttonCancel.title = NSLocalizedString(@"Cancel", nil);
    buttonSend.title = NSLocalizedString(@"Send", nil);
}

- (void)viewDidLoad {
    // Set notification for text view text change
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textChanged:) name:UITextViewTextDidChangeNotification object:nil];
    
    // Hide UI
    [self hideUI];
    
    // Init labels
    [self initLabels];
    
    // Set the corresponding suggested tweet
    textView.text = tweetSuggested;
    
    // Init the chars left for suggested tweet.
    int charsLeft = kTweetLenght - textView.text.length;
    labelCharsCount.text = [NSString stringWithFormat:@"%d", charsLeft];
    
}

- (void) viewDidAppear: (BOOL)animated {
    [super viewDidAppear:animated];
    
    [textView becomeFirstResponder];
    
	if (_engine) {
        
    } else {
        _engine = [[SA_OAuthTwitterEngine alloc] initOAuthWithDelegate: self];
        _engine.consumerKey = kOAuthConsumerKey;
        _engine.consumerSecret = kOAuthConsumerSecret;
        
        UIViewController *controller = [SA_OAuthTwitterController controllerToEnterCredentialsWithTwitterEngine: _engine delegate: self];
        controller.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        controller.modalPresentationStyle = UIModalPresentationFormSheet;
        
        if (controller) 
            [self presentModalViewController: controller animated: NO];
        else {
            [self showUI];
        }
    }
}

- (void)viewDidUnload {
    textView = nil;
    navigationBar = nil;
    buttonCancel = nil;
    buttonSend = nil;
    labelCharsCount = nil;
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        return (interfaceOrientation == UIInterfaceOrientationPortrait);
    } else {
        return YES;
    }
}

#pragma mark - Actions

- (IBAction)buttonCancelTouchUp:(id)sender {
    [self dismissModalViewControllerAnimated:NO];
}

- (IBAction)buttonSendTouchUp:(id)sender {
    [_engine sendUpdate: textView.text];
    
    // Progress HUD
    progressHUD = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:progressHUD];
    progressHUD.delegate = self;
    CGRect frame = CGRectMake(progressHUD.frame.origin.x, progressHUD.frame.origin.y-50, progressHUD.frame.size.width, progressHUD.frame.size.height);
    progressHUD.frame = frame;
    [progressHUD show:YES];
}

-(void)textChanged:(NSNotification*)notification {
    int charsLeft = kTweetLenght - textView.text.length;
    labelCharsCount.text = [NSString stringWithFormat:@"%d", charsLeft];
    
    if (charsLeft < 0) {
        [self disableSend];
    } else {
        [self enableSend];
    }
}

#pragma mark -
#pragma mark MBProgressHUDDelegate methods

- (void)hudWasHidden {
    
    [progressHUD removeFromSuperview];
    [progressHUD release];
}

@end
