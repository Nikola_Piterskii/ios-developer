
#import "DACircularProgressView.h"

#define DEGREES_2_RADIANS(x) ((M_PI / 180) * (x))
#define DAROUND_CORNERS_DEFAULT TRUE
@implementation DACircularProgressView

@synthesize trackTintColor = _trackTintColor;
@synthesize progressTintColor =_progressTintColor;
@synthesize roundedCorners = _roundedCorners;
@synthesize progress = _progress;

- (float)progress
{
    if (!_progress) {
        _progress = 0.001f;
    }
    return _progress;
}

- (id)init
{
    self = [super initWithFrame:CGRectMake(0.0f, 0.0f, 40.0f, 40.0f)];
    if (self)
    {
        self.backgroundColor = [UIColor clearColor];
        _roundedCorners = DAROUND_CORNERS_DEFAULT;
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        self.backgroundColor = [UIColor clearColor];
        _roundedCorners = DAROUND_CORNERS_DEFAULT;
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        self.backgroundColor = [UIColor clearColor];
        _roundedCorners = DAROUND_CORNERS_DEFAULT;
    }
    return self;
}

- (void)drawRect:(CGRect)rect
{    
    CGPoint centerPoint = CGPointMake(rect.size.height / 2, rect.size.width / 2);
    CGFloat radius = MIN(rect.size.height, rect.size.width) / 2;
    
    CGFloat pathWidth = radius * 0.3f;
    
    CGFloat radians = DEGREES_2_RADIANS((self.progress*359.9)-90);
    CGFloat xOffset = radius*(1 + 0.85*cosf(radians));
    CGFloat yOffset = radius*(1 + 0.85*sinf(radians));
    CGPoint endPoint = CGPointMake(xOffset, yOffset);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    //UIView *test = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
    
    //UIColor *temp = [UIColor colorWithRed:1.0f green:1.0f blue:1.0f alpha:0.9f];
    UIColor *temp = [UIColor clearColor];
    [temp setFill];
    CGMutablePathRef trackPath = CGPathCreateMutable();
    CGPathMoveToPoint(trackPath, NULL, centerPoint.x, centerPoint.y);
    CGPathAddArc(trackPath, NULL, centerPoint.x, centerPoint.y, radius, DEGREES_2_RADIANS(270), DEGREES_2_RADIANS(-90), NO);
    CGPathCloseSubpath(trackPath);
    CGContextAddPath(context, trackPath);
    CGContextFillPath(context);
    CGPathRelease(trackPath);
    
    temp = [UIColor colorWithRed:255.0f green:0.0f/255.0f blue:15.0f/255.0f alpha:1.0f];
    [self.progressTintColor setFill];
    [temp setFill];
    CGMutablePathRef progressPath = CGPathCreateMutable();
    CGPathMoveToPoint(progressPath, NULL, centerPoint.x, centerPoint.y);
    CGPathAddArc(progressPath, NULL, centerPoint.x, centerPoint.y, radius, DEGREES_2_RADIANS(270), radians, NO);
    CGPathCloseSubpath(progressPath);
    CGContextAddPath(context, progressPath);
    CGContextFillPath(context);
    CGPathRelease(progressPath);
    
  
    if(_roundedCorners == YES)// make rounded corners
    {
      CGContextAddEllipseInRect(context, CGRectMake(centerPoint.x - pathWidth/2, 0, pathWidth, pathWidth));
      CGContextFillPath(context);
    
      CGContextAddEllipseInRect(context, CGRectMake(endPoint.x - pathWidth/2, endPoint.y - pathWidth/2, pathWidth, pathWidth));
      CGContextFillPath(context);
    }    
    
    CGContextSetBlendMode(context, kCGBlendModeClear);;
    CGFloat innerRadius = radius * 0.93;
    CGPoint newCenterPoint = CGPointMake(centerPoint.x - innerRadius, centerPoint.y - innerRadius);    
    CGContextAddEllipseInRect(context, CGRectMake(newCenterPoint.x, newCenterPoint.y, innerRadius*2, innerRadius*2));
    CGContextFillPath(context);
}

#pragma mark - Property Methods

- (UIColor *)trackTintColor
{
    if (!_trackTintColor)
    {
       //UIColor *temp = [UIColor colorWithRed:1.0f green:1.0f blue:1.0f alpha:0.9f];
        //UIColor *temp1 = [[UIColor alloc  ]initWithRed:1.0f green:1.0f blue:1.0f alpha:0.9f];
       // _trackTintColor = [UIColor colorWithCGColor:temp1.CGColor];
        _trackTintColor = [UIColor redColor];
    }
    return _trackTintColor;
}

- (UIColor *)progressTintColor
{
    if (!_progressTintColor)
    {
        _progressTintColor = [UIColor redColor];
    }
    return _progressTintColor;
}

- (void)setProgress:(CGFloat)progress
{
    _progress = progress;
    [self setNeedsDisplay];
}

@end
