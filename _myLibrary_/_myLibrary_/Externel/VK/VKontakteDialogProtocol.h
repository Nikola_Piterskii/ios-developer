//
//  VKontakteDialogProtocol.h
//  iPuzzle
//
//  Created by skraevsky on 31.07.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol VKontakteDialogProtocol <NSObject>

@optional
-(void) vkSendToWallSuccess:(NSDictionary*)params;
-(void) vkSendToWallError:(NSDictionary*)params;

@end
