//
//  VKLoginViewController.m
//  iPuzzle
//
//  Created by skraevsky on 11.07.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "VKLoginViewController.h"

@interface VKLoginViewController ()

@property (nonatomic, retain) UIWebView* webView;
@end

@implementation VKLoginViewController

@synthesize delegate;
@synthesize webView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    self.webView = nil;
}

-(void) dealloc
{
    [webView release];
    [super dealloc];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortrait);
}

-(void) showLoginPage:(NSURL *)url
{
    CGSize main_window_size = [[UIScreen mainScreen] bounds].size;
    self.webView = [[[UIWebView alloc] initWithFrame:CGRectMake(0, 0, main_window_size.height,
                                                                main_window_size.width)] autorelease];
    self.webView.scalesPageToFit = YES;
    self.webView.delegate = self;
    self.view = self.webView;
    UIWindow* window = [UIApplication sharedApplication].keyWindow;
    [window.rootViewController presentModalViewController:self animated:YES];
    [self.webView loadRequest:[NSURLRequest requestWithURL:url]];
}

#pragma mark - UIWebViewDelegate
-(void) webViewDidFinishLoad:(UIWebView *)webView
{
    if ([self.delegate vkDidLogin:self.webView.request.URL])
        [self dismissModalViewControllerAnimated:YES];
}

-(void) webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [self dismissModalViewControllerAnimated:YES];
}

@end
