//
//  VKontakteSessionProtocol.h
//  iPuzzle
//
//  Created by skraevsky on 11.07.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol VKontakteSessionProtocol <NSObject>
-(void) vkDidLogin;
-(void) vkLoginFailed;
-(void) vkDidLogout;
@end
