//
//  VKontakte.m
//  iPuzzle
//
//  Created by skraevsky on 11.07.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "VKontakte.h"
#import "JSONKit.h"

static NSString* baseUrl = @"http://api.vk.com/";
static NSString* baseUrlS = @"https://api.vk.com/";
static NSString* authUrl = @"oauth/authorize?client_id=%@&scope=%@&redirect_uri=%@&display=touch&response_type=token";
static NSString* postToWallUrl = @"method/wall.post?owner_id=%@&access_token=%@&message=%@&attachment=%@";
static NSString* getWallUploadServerUrl = @"method/photos.getWallUploadServer?owner_id=%@&access_token=%@";
static NSString* savePhotoUrl = @"method/photos.saveWallPhoto?owner_id=%@&access_token=%@&server=%@&photo=%@&hash=%@";

static NSString* defaultsToken = @"vkToken";
static NSString* defaultsUserId = @"vkUserId";
static NSString* defaultsDateExpired = @"vkTokenExpired";

@interface VKontakte()

@property (nonatomic, retain) NSString* applicationId;
@property (nonatomic, retain) NSArray* applicationPermissions;
@property (nonatomic, retain) VKLoginViewController* loginController;
@property (nonatomic, retain) NSString* userId;

-(void) tryToAuthorize;
-(NSURL*) getAuthUrl;
-(NSDictionary*) getUrlArgs:(NSString*)str;
-(NSString*) ownBasedUrl;
-(NSString*) urlEncoding:(NSString*)src;

-(NSString*) getPhotoId:(NSArray*)image_prm;

-(NSDictionary*) sendRequest:(NSURL*)url;
-(NSDictionary*) getWallUploadServer;
-(NSDictionary*) uploadPhoto:(NSArray*)image url:(NSString*)url;
-(NSDictionary*) sendPostRequest:(NSURL*)url withData:(NSData*)data imageType:(NSString*)type;
-(NSDictionary*) savePhoto:(NSDictionary*)args;
@end

@implementation VKontakte

@synthesize delegate;
@synthesize accessToken;
@synthesize expirationDate;
@synthesize applicationId;
@synthesize applicationPermissions; 
@synthesize loginController;
@synthesize userId;

-(id) initWithAppId:(NSString *)appId
{
    self = [super init];
    if (self != nil)
    {
        applicationId = appId;
        [applicationId retain];
    }
    return self;
}

-(void) dealloc
{
    [loginController release];
    [accessToken release];
    [expirationDate release];
    [applicationPermissions release];
    [applicationId release];
    [super dealloc];
}

#pragma mark - public

-(void) checkUserDefaults
{
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    self.accessToken = [defaults objectForKey:defaultsToken];
    self.userId = [defaults objectForKey:defaultsUserId];
    self.expirationDate = [defaults objectForKey:defaultsDateExpired];
}

-(BOOL) isSessionValid
{
    return ((self.accessToken != nil) && (self.expirationDate != nil) &&
           ([self.expirationDate compare:[NSDate date]] == NSOrderedDescending));
}

-(void) authorize:(NSArray *)permissions
{
    self.applicationPermissions = permissions;
    [self tryToAuthorize];
}

-(BOOL) handleOpenURL:(NSURL *)url
{
    if (![[url absoluteString] hasPrefix:[self ownBasedUrl]])
        return NO;
    NSDictionary* args = [self getUrlArgs:url.fragment];
    NSString* token = [args objectForKey:@"access_token"];
    NSString* user_id = [args objectForKey:@"user_id"];
    NSNumber* expires_in = [args objectForKey:@"expires_in"];
    NSDate* expiration_date = [[NSDate date] dateByAddingTimeInterval:expires_in.intValue];
    if ((token == nil) || (user_id == nil))
    {
        [self.delegate vkLoginFailed];
        return YES;
    }
    [[NSUserDefaults standardUserDefaults] setValue:token forKey:defaultsToken];
    [[NSUserDefaults standardUserDefaults] setValue:user_id forKey:defaultsUserId];
    [[NSUserDefaults standardUserDefaults] setValue:expiration_date forKey:defaultsDateExpired];
    [[NSUserDefaults standardUserDefaults] synchronize];
    self.accessToken = token;
    self.userId = user_id;
    self.expirationDate = expiration_date;
    [self.delegate vkDidLogin];
    return YES;
}

-(void) sendToWall:(NSDictionary *)params delegate:(id<VKontakteDialogProtocol>) sendDelegate
{
    NSString* message = [params objectForKey:@"message"];
    NSString* link = [params objectForKey:@"link"];
    NSArray* image_prm = [params objectForKey:@"image"];
    NSString* photo_id = [self getPhotoId:image_prm];
    NSString* attachments = [NSString string];
    if (photo_id != nil)
        attachments = [attachments stringByAppendingFormat:@"%@", photo_id];
    // link must be used as last attachment
    if (link != nil)
        attachments = [attachments stringByAppendingFormat:@",%@", link];
    NSString* url = baseUrlS;
    url = [url stringByAppendingFormat:postToWallUrl, self.userId, self.accessToken,
           [self urlEncoding:message], attachments];
    NSDictionary* results = [self sendRequest:[NSURL URLWithString:url]];
    if (results != nil)
        [sendDelegate vkSendToWallSuccess:params];
    else [sendDelegate vkSendToWallError:params];
}

#pragma mark - private
-(void) tryToAuthorize
{
    self.loginController = [[[VKLoginViewController alloc] init] autorelease];
    self.loginController.delegate = self;
    [self.loginController showLoginPage:[self getAuthUrl]];
}

-(NSURL*) getAuthUrl
{
    NSString* url = baseUrl;
    NSString* perm = [self.applicationPermissions componentsJoinedByString:@","];
    url = [url stringByAppendingFormat:authUrl, self.applicationId, perm, [self ownBasedUrl]];
    return [NSURL URLWithString:url];
}

-(NSDictionary*) getUrlArgs:(NSString *)str
{
    NSMutableDictionary* args = [NSMutableDictionary dictionary];
    NSArray* pairs = [str componentsSeparatedByString:@"&"];
    for (NSString* p in pairs)
    {
        NSArray* kv = [p componentsSeparatedByString:@"="];
        NSString* key = [kv objectAtIndex:0];
        NSString* val = [[kv objectAtIndex:1]
                         stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        [args setValue:val forKey:key];
    }
    return args;
}

-(NSString*) ownBasedUrl
{
    return [NSString stringWithFormat:@"http://api.vk.com/blank.html"];
}
           
-(NSString*) urlEncoding:(NSString*)src
{
    NSString* out = (NSString*)CFURLCreateStringByAddingPercentEscapes(NULL,
                                                             (CFStringRef)src,
                                                             NULL,
                                                             (CFStringRef)@"!*'\"();:@&=+$,/?%#[]% ",
                                                             CFStringConvertNSStringEncodingToEncoding(NSUTF8StringEncoding));
    [out autorelease];
    return out;
}

-(NSString*) getPhotoId:(NSArray*)image_prm
{
    if (image_prm != nil)
    {
        // need to upload image to server
        NSDictionary* dict = [self getWallUploadServer];
        // 1) get upload url for image
        NSString* upload_url = [[dict objectForKey:@"response"] objectForKey:@"upload_url"];
        if (upload_url != nil)
        {
            // 2) upload photo
            dict = [self uploadPhoto:image_prm url:upload_url];
            if (dict != nil)
            {
                // 3) save photo
                dict = [self savePhoto:dict];
                dict = [[dict objectForKey:@"response"] lastObject];
                return [dict objectForKey:@"id"];
            }
        }
    }    
    return nil;
}

-(NSDictionary*) sendRequest:(NSURL *)url
{
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                      cachePolicy:NSURLRequestReloadIgnoringLocalCacheData
                                                   timeoutInterval:10.0]; 
    NSError* error = nil; 
    NSData* result = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:&error];
    if (error != nil)
    {
        DLog(@"vk sendRequest error:%@ %@", error, url);
        return nil;
    }
    if (result != nil)
    {
        JSONDecoder* jsonParser = [JSONDecoder decoder];
        id dict = [jsonParser objectWithData:result];
        if (![dict isKindOfClass:[NSDictionary class]]) return nil;
        return dict;
    }
    return nil;
}
         
-(NSDictionary*) getWallUploadServer
{
    NSString* url = baseUrlS;
    url = [url stringByAppendingFormat:getWallUploadServerUrl, self.userId, self.accessToken];
    return [self sendRequest:[NSURL URLWithString:url]];
}

-(NSDictionary*) uploadPhoto:(NSArray*)image_prm url:(NSString*)url
{
    UIImage* image = [image_prm objectAtIndex:0];
    NSString* image_type = [image_prm objectAtIndex:1];
    NSData* image_data = nil;
    if ([image_type isEqualToString:@"image/png"])
        image_data = UIImagePNGRepresentation(image);
    else image_data = UIImageJPEGRepresentation(image, 0.7);
    return [self sendPostRequest:[NSURL URLWithString:url] withData:(NSData*)image_data imageType:image_type];
}

-(NSDictionary*) sendPostRequest:(NSURL*)url withData:(NSData *)data imageType:(NSString *)type
{
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestReloadIgnoringLocalCacheData
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"POST"];
    
    [request addValue:@"8bit" forHTTPHeaderField:@"Content-Transfer-Encoding"];
    
    CFUUIDRef uuid = CFUUIDCreate(nil);
    NSString *uuidString = [(NSString*)CFUUIDCreateString(nil, uuid) autorelease];
    CFRelease(uuid);
    NSString *stringBoundary = [NSString stringWithFormat:@"0xKhTmLbOuNdArY-%@",uuidString];
    NSString *endItemBoundary = [NSString stringWithFormat:@"\r\n--%@\r\n",stringBoundary];
    
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", stringBoundary];
    
    [request setValue:contentType forHTTPHeaderField:@"Content-Type"];
    
    NSMutableData *body = [NSMutableData data];
    
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n",stringBoundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"Content-Disposition: form-data; name=\"photo\"; filename=\"photo.jpg\"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    NSString* content_type = [NSString stringWithFormat:@"Content-Type: %@\r\n\r\n", type];
    [body appendData:[content_type dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:data];
    [body appendData:[[NSString stringWithFormat:@"%@",endItemBoundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // Добавляем body к NSMutableRequest
    [request setHTTPBody:body];
    
    NSData *result = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
    if(result)
    {
        JSONDecoder* parser = [JSONDecoder decoder];
        id dict = [parser objectWithData:result];        
        if (![dict isKindOfClass:[NSDictionary class]]) return nil;        
        return dict;
    }
    return nil;
}

-(NSDictionary*) savePhoto:(NSDictionary*)args
{
    NSString* hash = [args objectForKey:@"hash"];
    NSString* photo = [self urlEncoding:[args objectForKey:@"photo"]];
    NSNumber* server = [args objectForKey:@"server"];
    NSString* url = baseUrlS;
    url = [url stringByAppendingFormat:savePhotoUrl, self.userId, self.accessToken, server, photo, hash];
    return [self sendRequest:[NSURL URLWithString:url]];
}
         
#pragma mark - VKLoginDelegate
-(BOOL) vkDidLogin:(NSURL *)result
{
    return [self handleOpenURL:result];
}
@end
