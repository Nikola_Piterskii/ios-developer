//
//  VKontakte.h
//  iPuzzle
//
//  Created by skraevsky on 11.07.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VKontakteSessionProtocol.h"
#import "VKontakteDialogProtocol.h"
#import "VKLoginViewController.h"

@interface VKontakte : NSObject<VKLoginDelegate>

@property (nonatomic, assign) id<VKontakteSessionProtocol> delegate;
@property (nonatomic, retain) NSString* accessToken;
@property (nonatomic, retain) NSDate* expirationDate;

-(id) initWithAppId:(NSString*)appId;

-(void) checkUserDefaults;
-(BOOL) isSessionValid;
-(void) authorize:(NSArray*)permissions;
-(BOOL) handleOpenURL:(NSURL*)url;
-(void) sendToWall:(NSDictionary*)params delegate:(id<VKontakteDialogProtocol>)sendDelegate;
@end
