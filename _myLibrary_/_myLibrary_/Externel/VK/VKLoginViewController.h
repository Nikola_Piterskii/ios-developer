//
//  VKLoginViewController.h
//  iPuzzle
//
//  Created by skraevsky on 11.07.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol VKLoginDelegate <NSObject>

-(BOOL) vkDidLogin:(NSURL*)result;

@end

@interface VKLoginViewController : UIViewController<UIWebViewDelegate>

@property (nonatomic, assign) id<VKLoginDelegate> delegate;

-(void) showLoginPage:(NSURL*)url;
@end
