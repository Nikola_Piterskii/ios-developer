//
//  categoryObject.m
//  _myLibrary_
//
//  Created by Алексей Иванков on 2/13/13.
//  Copyright (c) 2013 Алексей Иванков. All rights reserved.
//

#import "CategoryObject.h"

@implementation CategoryObject
@synthesize _idCategory;
@synthesize nameCategory;
@synthesize catalogCell;

-(void)dealloc
{
  [_idCategory release];
  [nameCategory release];
  [catalogCell release];
  [super dealloc];
}

@end
