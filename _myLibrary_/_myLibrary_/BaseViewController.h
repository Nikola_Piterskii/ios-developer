//
//  BaseViewController.h
//  _myLibrary_
//
//  Created by Алексей Иванков on 1/24/13.
//  Copyright (c) 2013 Алексей Иванков. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ChangeViewControllerDelegate.h"
#import "OpenViewDelegate.h"
#import "JsonObjectDelegate.h"

@interface BaseViewController : UIViewController
@property (assign) id<ChangeViewControllerDelegate> delegate;
@property (nonatomic,assign) id<JsonObjectDelegate> jsonDelegate;
@property (retain,nonatomic) IBOutlet UIView *navigationbar;
@property (retain,nonatomic) IBOutlet UIView *bottomBar;


+(void)setHomeControllerName :(NSString*)homeNameController;
+(void)setInformationControllerName :(NSString*)informationNameController;
+(void)setCatalogControllerName :(NSString*) catalogNameController;
+(void)setSelectControllerName :(NSString*) informationCaruselNameController;
+(NSString*)getHomeName;
+(NSString*)getCatalogName;
+(NSString*)getInformationName;
+(NSString*)getSelectName;
-(CGRect)getFrame;
-(void)setFrame;

@end
