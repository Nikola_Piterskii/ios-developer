//
//  Utils.h
//  _myLibrary_
//
//  Created by Алексей Иванков on 1/24/13.
//  Copyright (c) 2013 Алексей Иванков. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum  {
    Business =      6000,
    Weather =       6001,
    Utilities =     6002,
    Travel =        6003,
    Sport =         6004,
    SocialNetwork = 6005,
    HandBook  =     6006,
    Perfomance =    6007,
    PhotoandVideo = 6008,
    News  =         6009,
    Navigation  =   6010,
    Music =         6011,
    WayOfLife =     6012,
    HealthandFitness =  6013,
    Games =         6014,
    Finance =       6015,
    Entertainment = 6016,
    Education =     6017,
    Books =         6018,
    Medical =       6020,
    Kiosk =         6021,
    Catalogs  =     6022,
    FoodandDrink  = 6023,
    Other =         9999
}CategoryEnum;


@interface Utils : NSObject

+(BOOL) isIpad;
+(NSString*) cacheDirectory;
+(void) createCacheDirectory;
+(NSURL*) applicationDocumentsDirectory;

@end
