//
//  Utils.m
//  _myLibrary_
//
//  Created by Алексей Иванков on 1/24/13.
//  Copyright (c) 2013 Алексей Иванков. All rights reserved.
//

#import "Utils.h"

@implementation Utils

+(BOOL)isIpad
{
    UIDevice *device = [UIDevice currentDevice];
    if([device.name isEqualToString:@"iPad"]||[device.name isEqualToString:@"iPad Simulator"])
     {
         return YES;
     }
    return NO;
}

+(NSString*) cacheDirectory
{
    NSArray* pathArray = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString* path = [[pathArray objectAtIndex:0] stringByAppendingPathComponent:[[NSBundle mainBundle] bundleIdentifier]];
	return path;
}

+(void) createCacheDirectory
{
    NSString* path = [Utils cacheDirectory];
    if (![[NSFileManager defaultManager] fileExistsAtPath:path])
    {
        NSError* error = nil;
        [[NSFileManager defaultManager] createDirectoryAtPath:path withIntermediateDirectories:YES attributes:nil error:&error];
        if (error)
            DLog(@"Create cache directory failed: %@", error);
    }
}

+(NSURL*) applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

@end
