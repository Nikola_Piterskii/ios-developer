//
//  BaseCarouselCell.m
//  _myLibrary_
//
//  Created by Алексей Иванков on 1/28/13.
//  Copyright (c) 2013 Алексей Иванков. All rights reserved.
//

#import "BaseCarouselCell.h"

@implementation BaseCarouselCell
@synthesize coverImage;
@synthesize viewCell;
@synthesize delegate;
@synthesize nameDirectory;
@synthesize _idView;

-(void)dealloc
{
    [nameDirectory release];
    [coverImage release];
    [viewCell release];
    [_idView release];
    [super dealloc];
}


- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.delegate touchView:self];
}
/*
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
    }
    return self;
}
 */

/*
- (void)drawRect:(CGRect)rect
{
   // Drawing code
}
*/

@end
