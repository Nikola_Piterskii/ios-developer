//
//  CaruselInformationViewController.m
//  _myLibrary_
//
//  Created by Алексей Иванков on 1/29/13.
//  Copyright (c) 2013 Алексей Иванков. All rights reserved.
//

#import "SelectViewController.h"
#import "Constants.h"

@interface SelectViewController ()
{
    BOOL initializationOrientation;
}


-(void)changeView:(UIInterfaceOrientation)toInterfaceOrientation ;

@end

@implementation SelectViewController

@synthesize imageObject;
@synthesize summaryObject;
@synthesize curentLink;
@synthesize baseView;

@synthesize imageObject_p;
@synthesize summaryObject_p;
@synthesize curentLink_p;
@synthesize baseView_p;

@synthesize imageObject_l;
@synthesize summaryObject_l;
@synthesize curentLink_l;
@synthesize baseView_l;

@synthesize mountView;


- (void)dealloc {
    [imageObject release];
    [summaryObject release];
    [baseView release];
    
    [imageObject_p release];
    [summaryObject_p release];
    [baseView_p release];
    
    [imageObject_l release];
    [summaryObject_l release];
    [baseView_l release];
    
    [mountView release];
    [super dealloc];
}

-(id)init
{
    self = [super init];
    if(self)
        {
            
    }
    return self;
}

-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [self changeView:toInterfaceOrientation];
}

-(void)changeView:(UIInterfaceOrientation)toInterfaceOrientation
{
    if(UIInterfaceOrientationIsLandscape(toInterfaceOrientation) )
        {
            if(!initializationOrientation)
                {
                    self.imageObject = self.imageObject_l;
                    self.summaryObject = self.summaryObject_l;
                    }
            else
                {
                    [self.baseView removeFromSuperview];
                    self.imageObject_l.image = self.imageObject.image;
                    self.summaryObject_l.text = self.summaryObject.text;
                    }
            self.baseView = self.baseView_l ;
            }
    else
        {
            if(!initializationOrientation)
                {
                    self.imageObject = self.imageObject_p;
                    self.summaryObject = self.summaryObject_p;
                }
            else
                {
                    [self.baseView removeFromSuperview];
                    self.imageObject_p.image = self.imageObject.image;
                    self.summaryObject_p.text = self.summaryObject.text;
                }
            self.baseView = self.baseView_p;
          
            }
      [self.mountView addSubview:self.baseView];
    initializationOrientation = YES;
}

-(IBAction)onTouchHomeController:(id)sender
{
    [self.delegate nextViewControllerName:[BaseViewController getHomeName] animation:UIViewAnimationOptionTransitionCrossDissolve ];
    [[NSNotificationCenter defaultCenter] postNotificationName:REFRESH_THE_CAROUSEL object:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:SET_CONTENT_FRAME object:nil];
}

-(void)viewDidLoad
{
    [super viewDidLoad];
    self.summaryObject.editable = NO;
    self.summaryObject_l.editable = NO;
    self.summaryObject_p.editable = NO;
    self.summaryObject.textAlignment = NSTextAlignmentJustified;
    self.summaryObject_l.textAlignment = NSTextAlignmentJustified;
    self.summaryObject_p.textAlignment = NSTextAlignmentJustified;
    [self changeView:[[UIDevice currentDevice] orientation]];
}


- (void)viewDidUnload {
    [self setImageObject:nil];
    [self setSummaryObject:nil];
    [super viewDidUnload];
}

-(void)sendJsonObject:(JsonObject *)jsonObject
{
    self.summaryObject.text = jsonObject.descriptionObject;
    self.imageObject.image = jsonObject.backgroundImageObject;
    self.curentLink = jsonObject.referenceObject;
    [self changeView:[[UIDevice currentDevice] orientation]];
}

-(void)sendApplicationObject:(Application *)application{
    self.summaryObject.text = application.descriptionApplication;
    
    NSString* pathToImage = [NSString stringWithFormat:@"%@/%@.png",[Utils cacheDirectory], application.id];
    self.imageObject.image = [UIImage imageWithContentsOfFile:pathToImage];
    self.curentLink = application.referencesItunes;
    [self changeView:[[UIDevice currentDevice] orientation]];
}

-(IBAction)onTouchCurentLink:(id)sender
{
    NSURL *url = [NSURL URLWithString:self.curentLink];
    UIApplication *apllication = [UIApplication sharedApplication];
    [apllication openURL:url];
}

-(void)setFrame
{
    self.baseView.frame = [self getFrame];
}

@end