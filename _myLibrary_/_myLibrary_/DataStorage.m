//
//  DataStorage.m
//  _myLibrary_
//
//  Created by Николай Голдин on 22.02.13.
//  Copyright (c) 2013 Алексей Иванков. All rights reserved.
//

#import "DataStorage.h"

@interface DataStorage()

-(Genre*)createGenre;
-(Application*)createApplication;
-(BOOL)isApplicationId:(NSInteger)applicationID;
-(BOOL)isGenreId:(NSInteger)genreID;

@end


@implementation DataStorage

@synthesize managedObjectContext;

#pragma mark - Override

-(id)init{
    self = [super init];
    if (self){
        
    }
    return self;
}

#pragma mark - Public methods

static DataStorage* _shared_data_storage = nil;
+(DataStorage*)sharedDataStorage{
    
    @synchronized(self){
        if (_shared_data_storage == nil)
            _shared_data_storage = [[DataStorage alloc] init];
    }
    return _shared_data_storage;
}

-(void) saveContext
{
    if ([self.managedObjectContext hasChanges])
    {
        [self.managedObjectContext processPendingChanges];
        
        NSError* error = nil;
        [self.managedObjectContext save:&error];
        if (error){
            DLog(@"[DataStorage saveContext] error: %@", error);
        }
    }
}

-(NSFetchedResultsController*)genreFetchedResultsController{
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    
    NSSortDescriptor *sortID = [[[NSSortDescriptor alloc] initWithKey:@"genreId" ascending:YES] autorelease];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"Genre" inManagedObjectContext:self.managedObjectContext];
    
    [request setEntity:entity];
    [request setSortDescriptors:[NSArray arrayWithObject:sortID]];
    
    NSFetchedResultsController *controller = [[[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:self.managedObjectContext sectionNameKeyPath:nil cacheName:nil] autorelease];
    
    [request release];
    
    NSError* error = nil;
    if (![controller performFetch:&error]){
        DLog(@"No return genreFetchedREsultsController");
        return nil;
    }
    
    return controller;
}

-(NSFetchedResultsController*)applicationsFetchedResultsController{
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    
    NSSortDescriptor *sortID = [[[NSSortDescriptor alloc] initWithKey:@"id" ascending:YES] autorelease];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Application" inManagedObjectContext:self.managedObjectContext];
    
    [request setEntity:entity];
    [request setSortDescriptors:[NSArray arrayWithObject:sortID]];
    
    NSFetchedResultsController *controller = [[[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:self.managedObjectContext sectionNameKeyPath:nil cacheName:nil] autorelease];
    
    [request release];
    
    NSError* error = nil;
    if (![controller performFetch:&error]){
        DLog(@"No return applicationsFetchedResultsController");
        return nil;
    }
    
    return controller;
}

-(void)addGenre:(NSString*)namegenre withId:(CategoryEnum)genreId{
    
    if ([self isGenreId:genreId])
        return;
    
    Genre* genre = [self createGenre];
    genre.genreId = [NSNumber numberWithInteger:genreId];
    genre.genreName = namegenre;
    
    [self saveContext];
}

-(void)addApplication:(JsonObject*)jsonObject{
    
    NSInteger applicationID = [jsonObject._id integerValue];
    NSInteger genreID = [jsonObject._idCategory integerValue];
    
    if ([self isApplicationId:applicationID])
        return;
    
    Application* application = [self createApplication];
    
    application.id = [NSNumber numberWithInteger:applicationID];
    application.applicationName = jsonObject.nameObject;
    application.referencesItunes = jsonObject.referenceObject;
    application.referencesImage = jsonObject.referenceBackgroundImageObject;
    application.descriptionApplication = jsonObject.descriptionObject;
    
    Genre* genre = [self genre:genreID];
    
    if (genre == nil){
        DLog(@"Return nil value for genreId");
        return;
    }
    
    application.genre = genre;
    
    [self saveContext];
}

-(Application*)application:(NSInteger)applicationID{
    NSFetchRequest* request = [[[NSFetchRequest alloc] init] autorelease];
    NSEntityDescription* entity = [NSEntityDescription entityForName:@"Application" inManagedObjectContext:self.managedObjectContext];
    [request setEntity:entity];
    
    NSSortDescriptor* sortDescriptor = [[[NSSortDescriptor alloc] initWithKey:@"id" ascending:NO] autorelease];
    [request setSortDescriptors:[NSArray arrayWithObject:sortDescriptor]];
    
    NSString* predicateString = @"id == %d";
    NSPredicate* predicate = [NSPredicate predicateWithFormat:predicateString, applicationID];
    [request setPredicate:predicate];
    
    NSError* error = nil;
    NSArray* result = [self.managedObjectContext executeFetchRequest:request error:&error];
    if (error){
        DLog(@"Failed return application");
        return nil;
    }
    
    NSInteger countApplication = [result count];
    
    if (countApplication == 0 || countApplication == 2){
        DLog(@"Entity error");
        return nil;
    }
    
    return [result objectAtIndex:0];
}

-(Genre*)genre:(NSInteger)genreID{
    NSFetchRequest* request = [[[NSFetchRequest alloc] init] autorelease];
    NSEntityDescription* entity = [NSEntityDescription entityForName:@"Genre" inManagedObjectContext:self.managedObjectContext];
    [request setEntity:entity];
    
    NSSortDescriptor* sortDescriptor = [[[NSSortDescriptor alloc] initWithKey:@"genreId" ascending:NO] autorelease];
    [request setSortDescriptors:[NSArray arrayWithObject:sortDescriptor]];
    
    NSString* predicateString = @"(genreId == %d) OR (genreId == %d)";
    NSPredicate* predicate = [NSPredicate predicateWithFormat:predicateString, genreID, Other];
    [request setPredicate:predicate];
    
    NSError* error = nil;
    NSArray* result = [self.managedObjectContext executeFetchRequest:request error:&error];
    if (error){
        DLog(@"Failed return genre");
        return nil;
    }
    
    NSUInteger count = [result count];
    
    switch (count) {
        case 0:
            return nil;
            break;
        case 1:
            return [result objectAtIndex:0];
            break;
        default:
            for (Genre* genre in result) {
                NSInteger index = [genre.genreId integerValue];
                if (index == genreID)
                    return genre;
            }
            return nil;
            break;
    }
}

-(NSArray*)applicationsFromGenreId:(NSInteger)genreID{
    NSFetchRequest* request = [[NSFetchRequest alloc] init];
    NSEntityDescription* entity = [NSEntityDescription entityForName:@"Application" inManagedObjectContext:self.managedObjectContext];
    [request setEntity:entity];
    [request setRelationshipKeyPathsForPrefetching:[NSArray arrayWithObjects:@"genre", nil]];
    
    NSSortDescriptor* sortDescriptor = [[[NSSortDescriptor alloc] initWithKey:@"id" ascending:NO] autorelease];
    NSArray* sortDescriptors = [NSArray arrayWithObjects:sortDescriptor, nil];
    [request setSortDescriptors:sortDescriptors];
    
    NSString* predicateString = @"genre.id == %d";
    NSPredicate* predicate = [NSPredicate predicateWithFormat:predicateString, genreID];
    [request setPredicate:predicate];
    
    NSError* error = nil;
    NSArray* result = [self.managedObjectContext executeFetchRequest:request error:&error];
    
    [request release];
    
    if (error){
        DLog(@"Failed return applicationsFromGenreId");
        return nil;
    }
    
    
    return result;
}

-(NSArray*)genresArray:(NSString*)tableName{
    
    NSFetchRequest* request = [[[NSFetchRequest alloc] init] autorelease];
    NSEntityDescription* entity = [NSEntityDescription entityForName:tableName inManagedObjectContext:self.managedObjectContext];
    [request setEntity:entity];
    
    NSError* error = nil;
    NSArray* result = [self.managedObjectContext executeFetchRequest:request error:&error];
    if (error){
        DLog(@"[DataStorage array] request error: %@", error);
        return nil;
    }
    return result;
}
-(NSArray*)applicationsArray:(NSString*)tableName{
    
    return [self genresArray:tableName];
}
-(NSDictionary*)genresDictionary:(NSString*)tableName{
    
    NSArray* arr = [self genresArray:tableName];
    NSMutableDictionary* dict = [NSMutableDictionary dictionary];
    
    for (Genre* genre in arr)
        [dict setObject:genre forKey:genre.genreId];
    
    return dict;
}
-(NSDictionary*)applicationDictionary:(NSString*)tableName{
    
    NSArray* arr = [self applicationsArray:tableName];
    NSMutableDictionary* dict = [NSMutableDictionary dictionary];
    
    for (Application* application in arr)
        [dict setObject:application forKey:application.id];
    
    return dict;
}

#pragma mark - Private methods

-(Genre*)createGenre{
    Genre* genre = [NSEntityDescription insertNewObjectForEntityForName:@"Genre" inManagedObjectContext:self.managedObjectContext];
    return genre;
}

-(Application*)createApplication{
    Application* application = [NSEntityDescription insertNewObjectForEntityForName:@"Application" inManagedObjectContext:self.managedObjectContext];
    return application;
}

-(BOOL)isApplicationId:(NSInteger)applicationID{
    
    NSFetchRequest* request = [[[NSFetchRequest alloc] init] autorelease];
    NSEntityDescription* entity = [NSEntityDescription entityForName:@"Application" inManagedObjectContext:self.managedObjectContext];
    [request setEntity:entity];
    
    NSSortDescriptor* sortDescriptor = [[[NSSortDescriptor alloc] initWithKey:@"id" ascending:NO] autorelease];
    NSArray* sortDescriptors = [NSArray arrayWithObjects:sortDescriptor, nil];
    [request setSortDescriptors:sortDescriptors];
    
    NSString* predicateString = @"id == %d";
    NSPredicate* predicate = [NSPredicate predicateWithFormat:predicateString, applicationID];
    [request setPredicate:predicate];
    
    NSError* error = nil;
    NSArray* result = [self.managedObjectContext executeFetchRequest:request error:&error];
    if (error){
        DLog(@"[Datastorage isApplicationId]");
        return NO;
    }
    
    if ([result count] == 0)
        return NO;
    
    return YES;
}

-(BOOL)isGenreId:(NSInteger)genreID{
    NSFetchRequest* request = [[[NSFetchRequest alloc] init] autorelease];
    NSEntityDescription* entity = [NSEntityDescription entityForName:@"Genre" inManagedObjectContext:self.managedObjectContext];
    [request setEntity:entity];
    
    NSSortDescriptor* sortDescriptor = [[[NSSortDescriptor alloc] initWithKey:@"genreId" ascending:NO] autorelease];
    NSArray* sortDescriptors = [NSArray arrayWithObjects:sortDescriptor, nil];
    [request setSortDescriptors:sortDescriptors];
    
    NSString* predicateString = @"genreId == %d";
    NSPredicate* predicate = [NSPredicate predicateWithFormat:predicateString, genreID];
    [request setPredicate:predicate];
    
    NSError* error = nil;
    NSArray* result = [self.managedObjectContext executeFetchRequest:request error:&error];
    if (error){
        DLog(@"[Datastorage isGenreId]");
        return NO;
    }
    
    if ([result count] == 0)
        return NO;
    
    return YES;
}

@end

