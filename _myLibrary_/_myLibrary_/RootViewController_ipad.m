//
//  RootViewController_ipad.m
//  _myLibrary_
//
//  Created by Алексей Иванков on 1/24/13.
//  Copyright (c) 2013 Алексей Иванков. All rights reserved.
//

#import "RootViewController_ipad.h"
#import "SplashViewController_ipad.h"
#import "HomeScreeenViewController_ipad.h"
#import "InformationViewController_ipad.h"
#import "CategoryViewController_ipad.h"
#import "SelectViewController_ipad.h"



@interface RootViewController_ipad ()

@end

@implementation RootViewController_ipad
-(SplashViewController*)splashViewController
{
    if(_splashViewController==nil)
    {
        _splashViewController = [[SplashViewController_ipad alloc]initWithNibName:@"SplashViewController_ipad" bundle:nil];
    }
    return _splashViewController;
}

-(HomeScreeenViewController*)homeScreenViewController
{
    if(_homeScreenViewController==nil)
    {
        _homeScreenViewController = [[HomeScreeenViewController_ipad alloc]initWithNibName:@"HomeScreeenViewController_ipad" bundle:nil];
    }
    return _homeScreenViewController;
}

-(InformationViewController*)informationViewController
{
    if(_informationViewController==nil)
    {
        _informationViewController = [[InformationViewController_ipad alloc]initWithNibName:@"InformationViewController_ipad" bundle:nil];
    }
    return _informationViewController;
}

-(CategoryViewController*)catalogViewController
{
    if(_catalogViewController==nil)
    {
        _catalogViewController = [[CatalogViewController_ipad alloc]initWithNibName:@"CatalogViewController_ipad" bundle:nil];
    }
    return _catalogViewController;
}

-(SelectViewController*)selectViewController
{
   if(_selectViewController==nil)
   {
       _selectViewController = [[SelectViewController_ipad alloc]initWithNibName:nil bundle:nil];
   }
    return _selectViewController;
}

@end
