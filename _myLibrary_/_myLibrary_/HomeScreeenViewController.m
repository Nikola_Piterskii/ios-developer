//
//  HomeScreeenViewController.m
//  _myLibrary_
//
//  Created by Алексей Иванков on 1/24/13.
//  Copyright (c) 2013 Алексей Иванков. All rights reserved.
//

#import "HomeScreeenViewController.h"
#import "Constants.h"
#import "BaseViewController.h"
#import "BaseCarouselCell.h"
#import "CarouselCell_iphone.h"
#import "CategoryObject.h"
#import "Utils.h"
#import "SelectViewController.h"
#import "DownloadManagerObject.h"
#import "JsonObjectDelegate.h"
#import "DataStorage.h"


@interface HomeScreeenViewController ()
{
     BOOL isEndScroll;
     DownloadManagerObject *downloadManager;
     NSInteger countView;
    NSUInteger indexToCarousel;
}
@property (nonatomic, strong) NSMutableArray *itemsForCarousel;
@property (nonatomic,retain) NSMutableArray *items;
@property (nonatomic, retain) NSMutableDictionary *hashMapObject;


-(NSUInteger)getNumberVisibleItems;
-(CGSize) sizeItemView;
-(void)rotateCarousel:(NSNotification*)notification;
-(void)loadContent;
-(void)setFrameView :(UIView*)view;
-(void)refreshCarousel;
-(void)setContentOffSetForView;

@end

@implementation HomeScreeenViewController
@synthesize itemsForCarousel;
@synthesize carousel = _carousel;
@synthesize carouselCell = _carouselCell;
@synthesize items;
@synthesize hashMapObject;
@synthesize fetchedResultsController;

-(NSFetchedResultsController*)fetchedResultsController{
    if (fetchedResultsController == nil){
        fetchedResultsController = [[[DataStorage sharedDataStorage] applicationsFetchedResultsController] retain];
    }
    return fetchedResultsController;
}


-(void)dealloc
{
    [itemsForCarousel release];
    [_carousel release];
    [_carouselCell release];
    [items release];
    [hashMapObject release];
    [fetchedResultsController release];
    [super dealloc];
}

#pragma mark -
#pragma mark iCarousel methods

-(NSUInteger)numberOfItemsInCarousel:(iCarousel *)carousel 
{
    return  [[[self.fetchedResultsController sections] objectAtIndex:0] numberOfObjects];
    //return  [self.itemsForCarousel count];
}

-(UIView*)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSUInteger)index reusingView:(UIView *)view //загрузка view в карусель
{
    Application* application = [self.fetchedResultsController objectAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0]];
    
    NSString* nameView = nil;
    if ([Utils isIpad]){
        nameView = @"CarouselCell_ipad";
    }else{
        nameView = @"CarouselCell_iphone";
    }
    
    UIView *createView = (BaseCarouselCell*)[[[NSBundle mainBundle] loadNibNamed:nameView owner:self options:nil] objectAtIndex:0];
    
    view = createView;
    
    ((BaseCarouselCell*)view)._idView = application.id;
    [self setFrameView:view];
    ((BaseCarouselCell*)view).delegate = self;
    ((BaseCarouselCell*)view).nameDirectory.text = [NSString stringWithFormat:@"%@", application.applicationName];
    
    NSString* pathForImage = [NSString stringWithFormat:@"%@/%@.png",[Utils cacheDirectory], application.id];
    ((BaseCarouselCell*)view).coverImage.image = [UIImage imageWithContentsOfFile:pathForImage];
    
    return view;
}

-(void)setFrameView :(UIView*)view
{
     CGFloat widthBar;
     if(UIInterfaceOrientationIsLandscape([[UIDevice currentDevice] orientation] ))
     {
         widthBar = HEIGHT_BOTTOM_BAR*2.5;
         //self.carousel.contentOffset = CGSizeMake(0, -HEIGHT_BOTTOM_BAR*1.5);
     }
     else
     {
         widthBar = HEIGHT_BOTTOM_BAR*4;
         //self.carousel.contentOffset = CGSizeMake(0, HEIGHT_BOTTOM_BAR/4);
     }
     CGFloat heightView = HEIGHT_SELF_VIEW - widthBar;
     CGFloat widthView =  HEIGHT_SELF_VIEW /3;
     view.frame = CGRectMake(0, 0, widthView, heightView);
}

-(void)touchView:(UIView *)sender //обработчик события тача view карусели
{
    if(!isEndScroll)
        return;
    isEndScroll = NO;
    
    BaseCarouselCell *curentView = (BaseCarouselCell*)sender;
    NSInteger appID = [curentView._idView integerValue];
    Application* application = [[DataStorage sharedDataStorage] application:appID];
    
    [self.carousel setScrollEnabled:NO];
    curentView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1, 1);
    [UIView animateWithDuration:1.0f animations:^{
        curentView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.5, 1.5);
        [self.delegate nextViewControllerName:[SelectViewController getSelectName] animation:UIViewAnimationOptionTransitionCrossDissolve];
        [self.jsonDelegate sendApplicationObject:application];
    }completion:^(BOOL finished) {
        [self.carousel setScrollEnabled:YES];
        curentView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1, 1);
    }];
}

-(CGFloat)carousel:(iCarousel *)carousel valueForOption:(iCarouselOption)option withDefault:(CGFloat)value  //установка параметров карусели.
{
    switch (option) {
        case iCarouselOptionWrap: //замкнуть карусель
               return YES;
            break;
            
        case iCarouselOptionShowBackfaces: // отображаение задней стороны элементов карусели
            return YES;
            break;
            
        case iCarouselOptionVisibleItems: //количество видимых элементов карусели
            return 15;
            break;
            
        case iCarouselOptionFadeMin:
            return -0.2f;
            break;
            
        case iCarouselOptionFadeMax:
            return 0.2f;
            break;
            
        case iCarouselOptionFadeRange:
            return 2;
            break;
            
        case iCarouselOptionArc: //манипуляции с радиусом
            return 5;
            break;
            
        case iCarouselOptionSpacing: // расстояние между элементами карусели
            return value*=0.5f;
            break;
    
        default:
            return value; 
            break;
    }
}

-(void)carouselWillBeginScrollingAnimation:(iCarousel *)carousel //вызывается при старте анимациим
{
}

-(void)carouselDidEndScrollingAnimation:(iCarousel *)carousel //вызывается при окончании анимации
{
     isEndScroll = YES;
}

/*
-(CGFloat)carouselItemWidth:(iCarousel *)carousel //возвращает ширину элементов карусели
{

}

-(CATransform3D)carousel:(iCarousel *)carousel itemTransformForOffset:(CGFloat)offset baseTransform:(CATransform3D)transform //пользовательская трансформация карусели
{

}
 */

/*
-(void)carousel:(iCarousel *)carousel didSelectItemAtIndex:(NSInteger)index////selected view
{
     DLog(@"didSelectItemAtIndex");
}
*/

#pragma mark -
#pragma mark HomeScreenlViewController methods

-(NSMutableArray*)itemsForCarousel
{
 if (itemsForCarousel==nil)
 {
     itemsForCarousel = [[NSMutableArray alloc]init];
 }
    return itemsForCarousel;
}

-(NSMutableDictionary*)hashMapObject
{
  if(hashMapObject==nil)
  {
      hashMapObject = [[NSMutableDictionary alloc]init];
  }
    return hashMapObject;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
         downloadManager = [DownloadManagerObject getInstance];
         downloadManager.jsonObjectDelegate = self;
         downloadManager.fileRequest.imageDelegate = self;
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(rotateCarousel:) name:ROTATE_THE_CAROUSEL object:nil];
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(refreshCarousel) name:REFRESH_THE_CAROUSEL object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setContentOffSetForView) name:SET_CONTENT_FRAME object:nil];
        [downloadManager startLoadContent:URL_SERVER_RSS countObject:50];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
     self.carousel.type = iCarouselTypeRotary;
    self.carousel.scrollSpeed = 0.4;
     self.carousel.perspective = -0.005; // установка значение перспективы до -0,01
    [self refreshCarousel];    
    //self.carousel.contentOffset = CGSizeMake(50, 50); перемещение карусели
    //self.carousel.viewpointOffset = CGSizeMake(10, 10); смещение противоположное content offset
    //self.carousel.decelerationRate = 0.98f; // скорость замедления карусели после выбора
    //self.carousel.bounceDistance = 0.1f; //величина на которую карусель будет смещаться после после прокрутки. max = 1
    //self.carousel.scrollEnabled = NO; включение и выключение прокрутки карусели
    //self.carousel.contentView added view and carousel in subview
    //self.carousel.scrollOffset  количесво прокручиваемых элементов в карусели
    //self.carousel.scrollSpeed = 0.9;// скорость прокрутки(max 1.0)
    //self.carousel.vertical  вертикальная карусель или горизонтальная
    //self.carousel.isDragging возвращает да если карусель еще прокручивается
}

-(void)viewDidUnload
{
    [super viewDidUnload];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:ROTATE_THE_CAROUSEL object:nil];
}

-(void)rotateCarousel:(NSNotification*)notification
{
    if (notification == nil)
        return;
    
    id dictionary = notification.object;
    
    if ([dictionary isKindOfClass:[NSDictionary class]]){
        
        NSArray* applications = [[DataStorage sharedDataStorage] applicationsArray:@"Application"];
        
        for (Application *object in applications) {
            NSString* idCategoryObject = [NSString stringWithFormat:@"%@", [dictionary valueForKey:@"GENRE_ID"]];
            NSString* idJsonObject = [NSString stringWithFormat:@"%@", [object.genre valueForKey:@"genreId"]];
            
            if ([idCategoryObject isEqualToString:idJsonObject]){
                
                id index = [self.hashMapObject valueForKey:[NSString stringWithFormat:@"%@", object.id]];
                [self.carousel scrollToOffset:[index floatValue] duration:2];
                break;
            }
        }
    }
       //self.carousel scrollToItemAtIndex:<#(NSInteger)#> animated:<#(BOOL)#> перемещение карусели на указанный индекс
       //self.carousel scrollByNumberOfItems:<#(NSInteger)#> duration:<#(NSTimeInterval)#> перемещение карусели на фиксированное расстояние
       //self.carousel itemViewAtIndex:<#(NSInteger)#>  возвращает видимый элемент карусели или не вернет ничего если элемент не отображается
       //self.carousel offsetForItemAtIndex: Возвращает смещение для указанного индекса элемента itemWidth от центра
}

-(void)refreshCarousel
{
    [self.carousel scrollByOffset:3 duration:0];
    [self.carousel scrollByOffset:-3 duration:0];
    [self.carousel reloadData];
}

-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [self setContentOffSetForView];
    //[self refreshCarousel];
}

-(void)setContentOffSetForView{
    if(UIInterfaceOrientationIsLandscape([[UIDevice currentDevice] orientation]))
    {
        self.carousel.contentOffset = CGSizeMake(0, -HEIGHT_BOTTOM_BAR*1.5);
    }
    else
    {
        self.carousel.contentOffset = CGSizeMake(0, HEIGHT_BOTTOM_BAR/4);
    }
    [self refreshCarousel];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)jsonObjectDownload:(JsonObject *)jsonObject
{
    
    [[DataStorage sharedDataStorage] addApplication:jsonObject];
    DLog(@"%@", jsonObject._id);
    [self.hashMapObject setValue:[NSNumber numberWithInteger:indexToCarousel] forKey:[NSString stringWithFormat:@"%@", jsonObject._id]];
    indexToCarousel++;
    
    [self refreshCarousel];
}

#pragma mark - ImageDownloadDelegate

-(void)imageDownloadRequestSuccess:(NSString *)imageId
{
    /* NSNumber *number = [self.hashMapObject valueForKey:imageId];
     JsonObject *jsonObject = [self.itemsForCarousel  objectAtIndex: number.unsignedIntValue];
     NSString *pathCashDirectory = [Utils cacheDirectory];
    NSData *data = [NSData dataWithContentsOfFile:[NSString stringWithFormat:@"%@/%@.png",pathCashDirectory,jsonObject._id]];
    jsonObject.backgroundImageObject = [UIImage imageWithData:data];
    [self.itemsForCarousel replaceObjectAtIndex:number.unsignedIntValue withObject:jsonObject];*/
}

- (IBAction)ontouchInformation:(id)sender {
    [self.delegate nextViewControllerName: [BaseViewController getInformationName] animation:UIViewAnimationOptionTransitionCurlDown ];
}

- (IBAction)onTouchCatalog:(id)sender {
    [self.delegate nextViewControllerName: [BaseViewController getCatalogName] animation:UIViewAnimationOptionTransitionFlipFromLeft ];
}

@end
