//
//  RootViewController.m
//  _myLibrary_
//
//  Created by Алексей Иванков on 1/24/13.
//  Copyright (c) 2013 Алексей Иванков. All rights reserved.
//

#import "RootViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "Constants.h"

@interface RootViewController ()
{
    BOOL isFinishedSplash,isPushBar;
}

//-(void)trasitionViewController:(BaseViewController *)controller animate:(UIViewAnimationOptions )animationOptions
-(void)removeSplash:(NSTimer*)timer;


@end

@implementation RootViewController
@synthesize splashViewController = _splashViewController;
@synthesize homeScreenViewController=_homeScreenViewController;
@synthesize informationViewController = _informationViewController;
@synthesize catalogViewController = _catalogViewController;
@synthesize selectViewController = _selectViewController;
@synthesize curentViewController;
@synthesize navigationStack;
@synthesize navigationBar;
@synthesize baseViewController;
@synthesize myApplication;

-(void)dealloc
{
    [_splashViewController release];
    [_homeScreenViewController release];
    [_informationViewController release];
    [_catalogViewController release];
    [_selectViewController release];
    [curentViewController release];
    [navigationStack release];
    [navigationBar release];
    [baseViewController release];
    [myApplication release];
    [super dealloc];
}

-(NavigationStack*)navigationStack
{
  if(navigationStack==nil)
  {
      navigationStack = [[NavigationStack alloc]init];
  }
    return navigationStack;
}

-(BaseViewController*)baseViewController
{
  if(baseViewController==nil)
  {
      baseViewController = [[BaseViewController alloc]init];
  }
    return baseViewController;
}

-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if(self)
    {
        [self.navigationStack addViewControllerStack:self.catalogViewController];
        [self.navigationStack addViewControllerStack:self.selectViewController];
        [self.navigationStack addViewControllerStack:self.informationViewController];
        [self.navigationStack addViewControllerStack:self.homeScreenViewController];
         self.curentViewController = self.navigationStack.curentViewControllerStack;
         self.curentViewController.delegate = self;
        self.curentViewController.jsonDelegate = self.selectViewController;
        [BaseViewController setCatalogControllerName:[NSString stringWithFormat:@"%@", [self.catalogViewController class] ]];
        [BaseViewController setInformationControllerName:[NSString stringWithFormat:@"%@",[self.informationViewController class]]];
        [BaseViewController setHomeControllerName :[NSString stringWithFormat:@"%@",[self.homeScreenViewController class]]];
        [BaseViewController setSelectControllerName:[NSString stringWithFormat:@"%@",[self.selectViewController class]]];
        self.myApplication = [UIApplication sharedApplication];
    }
    return self;
}

-(void)viewDidLoad
{
    [super viewDidLoad];
    self.baseViewController.view.frame = self.view.bounds;
    [self.view addSubview:self.baseViewController.view];
    self.splashViewController.view.frame = self.baseViewController.view.bounds;
    [self.baseViewController.view addSubview:self.splashViewController.view];
    [NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(removeSplash:) userInfo:self.curentViewController repeats:NO];
}

-(void)viewDidUnload
{
    [super viewDidUnload];
    self.splashViewController = nil;
    self.homeScreenViewController = nil;
    self.informationViewController = nil;
    self.catalogViewController = nil;
    self.curentViewController = nil;
    self.selectViewController = nil;
    self.navigationStack = nil;
}

-(void)nextViewControllerName:(NSString *)nameViewController animation:(UIViewAnimationOptions)animationOptions 
{
     [self.navigationStack changeViewController:nameViewController];
     [self trasitionViewController:self.navigationStack.curentViewControllerStack animate:animationOptions];
}

-(void)trasitionViewController:(BaseViewController *)controller animate:(UIViewAnimationOptions )animationOptions
{
    controller.view.frame = self.curentViewController.view.frame;
    [UIView animateWithDuration:0.5 animations:^{
        
        self.curentViewController.navigationbar.frame = CGRectMake(self.curentViewController.view.frame.size.width, 0, self.curentViewController.navigationbar.frame.size.width, self.curentViewController.navigationbar.frame.size.height);
        self.curentViewController.bottomBar.frame = CGRectMake(-self.curentViewController.view.frame.size.width, self.curentViewController.view.frame.size.height -   self.curentViewController.bottomBar.frame.size.height, self.curentViewController.bottomBar.frame.size.width, self.curentViewController.bottomBar.frame.size.height);
       
    } completion:^(BOOL finished){
        
        if([controller respondsToSelector:@selector(setFrame)])
        {[controller setFrame];}
        
     
        [UIView transitionWithView:self.baseViewController.view duration:0.5 options:animationOptions animations:^
         {
                        
             controller.navigationbar.frame = CGRectMake(self.curentViewController.navigationbar.frame.size.width, 0, self.curentViewController.navigationbar.frame.size.width, self.curentViewController.navigationbar.frame.size.height);
             controller.bottomBar.frame = CGRectMake(-self.curentViewController.navigationbar.frame.size.width, 0, self.curentViewController.navigationbar.frame.size.width, self.curentViewController.navigationbar.frame.size.height);
           

             [self.baseViewController.view addSubview:controller.view];
         }
                        completion:^(BOOL stop)
         {
             
             CATransition *transition = [CATransition animation];
             transition.type = kCATransitionMoveIn;
             transition.duration = 0.3;
             transition.repeatCount = 1;
             transition.subtype = kCATransitionFromRight;
             transition.fillMode = kCAFillModeForwards;
             [controller.navigationbar.layer addAnimation:transition forKey:nil];
             transition.subtype = kCATransitionFromLeft;
             [controller.bottomBar.layer addAnimation:transition forKey:nil];
             controller.navigationbar.frame = CGRectMake(0, 0, self.curentViewController.navigationbar.frame.size.width, self.curentViewController.navigationbar.frame.size.height);
             controller.bottomBar.frame = CGRectMake(0, controller.view.frame.size.height -   self.curentViewController.bottomBar.frame.size.height, self.curentViewController.bottomBar.frame.size.width, self.curentViewController.bottomBar.frame.size.height);
             [self.baseViewController.view addSubview:controller.navigationbar];
             [self.baseViewController.view addSubview:controller.bottomBar];
             [self.curentViewController.view removeFromSuperview];
             self.curentViewController = controller;
             self.curentViewController.delegate = self;
         }];
    }];
}

-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    return YES;
}

/*-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskAll;
}*/

-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
    [self.curentViewController willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
}

-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [super didRotateFromInterfaceOrientation:fromInterfaceOrientation];
    [self.curentViewController didRotateFromInterfaceOrientation:fromInterfaceOrientation];
}

-(void)removeSplash:(NSTimer *)timer
{
    if([timer.userInfo isKindOfClass:[BaseViewController class]])
    {
        BaseViewController *controller = timer.userInfo;
        self.curentViewController.view.frame = self.splashViewController.view.bounds;
        self.curentViewController.navigationbar.frame = CGRectMake(0, 0, self.splashViewController.view.frame.size.width, self.curentViewController.navigationbar.frame.size.height);
        self.curentViewController.bottomBar.frame = CGRectMake(0, controller.view.frame.size.height - self.curentViewController.bottomBar.frame.size.height, self.splashViewController.view.frame.size.width, self.curentViewController.bottomBar.frame.size.height);
        [self.baseViewController.view addSubview:self.curentViewController.view];
        [self.baseViewController.view addSubview:controller.navigationbar];
        [self.baseViewController.view addSubview:controller.bottomBar];
        [self.splashViewController.view removeFromSuperview];
    }
    [timer invalidate];
}

@end
