//
//  AppDelegate_ipad.m
//  _myLibrary_
//
//  Created by Алексей Иванков on 1/24/13.
//  Copyright (c) 2013 Алексей Иванков. All rights reserved.
//

#import "AppDelegate_ipad.h"
#import "RootViewController_ipad.h"

@implementation AppDelegate_ipad

-(RootViewController*)rootViewController
{
    if(_rootViewController==nil)
    {
        _rootViewController = [[RootViewController_ipad alloc]init];
    }return _rootViewController;
}

@end
