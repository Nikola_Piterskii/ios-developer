//
//  Constants.h
//  _myLibrary_
//
//  Created by Алексей Иванков on 1/24/13.
//  Copyright (c) 2013 Алексей Иванков. All rights reserved.
//


#define HOME @"HomeScreeenViewController_iphone"
#define INFORMATION @"InformationViewController_iphone"
#define CATALOG @"CatalogViewController_iphone"
#define X_NAVIGATION_BAR 0
#define HEIGHT_NAVIGATION_BAR self.navigationbar.frame.size.height
#define HEIGHT_BOTTOM_BAR self.bottomBar.frame.size.height
#define X_SELF_VIEW self.view.frame.origin.x
#define Y_SELF_VIEW self.view.frame.origin.y
#define HEIGHT_SELF_VIEW  self.view.frame.size.height
#define WIDTH_SELF_VIEW  self.view.frame.size.width
#define ROTATE_THE_CAROUSEL @"ROTATE_THE_CAROUSEL"
#define REFRESH_THE_CAROUSEL @"REFRESH_THE_CAROUSEL"
#define SET_CONTENT_FRAME @"SET_CONTENT_FRAME"
#define EXPANSION @"%@.png"

////////JSON
#define SHOW_ABOUT_NOTIFICATION @"SHOW_ABOUT_NOTIFICATION"
#define CLOSE_ABOUT_NOTIFICATION @"CLOSE_ABOUT_NOTIFICATION"
#define FILE_FOR_SAVE_FILE_STREAM @"/Users/ngoldin/Desktop/testStream/first.png"
#define FILE_FOR_SAVE_IMAGE_STREAM @"/Users/local/NEOLINE-VRN/aivankov/Desktop/testImage/"
#define TOP_APPLICATION_COUNT 50
#define APPLE_FEED                      @"feed"
#define APPLE_LINK                      @"APPLE_LINK"
#define APPLE_ENTRY                     @"entry"
#define APPLE_ID                        @"APPLE_ID"
#define APPLE_ICON                      @"APPLE_ICON"
#define APPLE_AUTHOR                    @"APPLE_AUTHOR"
#define APPLE_RIGHTS                    @"APPLE_RIGHTS"
#define APPLE_TITLE                     @"APPLE_TITLE"
#define APPLICATION_CATEGORY            @"APPLICATION_CATEGORY"
#define APPLICATION_ID                  @"id"
#define APPLICATION_LINK                @"link"
#define APPLICATION_RIGHTS              @"APPLICATION_RIGHTS"
#define APPLICATION_SUMMARY             @"summary"
#define APPLICATION_TITLE               @"APPLICATION_TITLE"
#define APPLICATION_IMAGE_ARTIST        @"APPLICATION_IMAGE_ARTIST"
#define APPLICATION_IMAGE_PRICE         @"APPLICATION_IMAGE_PRICE"
#define APPLICATION_IMAGE_CONTENT_TYPE  @"APPLICATION_IMAGE_CONTENT_TYPE"
#define APPLICATION_IMAGE_RELEASE_DATE  @"APPLICATION_IMAGE_RELEASE_DATE"
#define APPLICATION_IMAGE_IMAGE         @"APPLICATION_IMAGE_IMAGE"
#define URL_SERVER_RSS                  @"https://itunes.apple.com/ru/rss/topfreeapplications/limit=%d/json"
/////////









