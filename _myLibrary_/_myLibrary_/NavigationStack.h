//
//  NavigationStack.h
//  _myLibrary_
//
//  Created by Алексей Иванков on 1/24/13.
//  Copyright (c) 2013 Алексей Иванков. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseViewController.h"

@interface NavigationStack : NSObject
@property (retain,nonatomic) BaseViewController *curentViewControllerStack;

-(void)addViewControllerStack :(UIViewController*)controller;
-(void)changeViewController :(NSString*)nameViewController;

@end
