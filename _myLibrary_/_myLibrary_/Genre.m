//
//  Genre.m
//  _myLibrary_
//
//  Created by Николай Голдин on 22.02.13.
//  Copyright (c) 2013 Алексей Иванков. All rights reserved.
//

#import "Genre.h"
#import "Application.h"


@implementation Genre

@dynamic genreId;
@dynamic genreName;
@dynamic application;

@end
