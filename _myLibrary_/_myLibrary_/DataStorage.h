//
//  DataStorage.h
//  _myLibrary_
//
//  Created by Николай Голдин on 22.02.13.
//  Copyright (c) 2013 Алексей Иванков. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Genre.h"
#import "Application.h"
#import "JsonObject.h"

@interface DataStorage : NSObject{
    NSManagedObjectContext* managedObjectContext;
}

@property (retain, nonatomic) NSManagedObjectContext* managedObjectContext;

+(DataStorage*)sharedDataStorage;
-(void) saveContext;

-(NSFetchedResultsController*)genreFetchedResultsController;
-(NSFetchedResultsController*)applicationsFetchedResultsController;

-(Genre*)genre:(NSInteger)genreID;
-(Application*)application:(NSInteger)applicationID;

-(void)addGenre:(NSString*)namegenre withId:(CategoryEnum)genreId;
-(void)addApplication:(JsonObject*)jsonObject;

-(NSArray*)applicationsFromGenreId:(NSInteger)genreID;
-(NSArray*)genresArray:(NSString*)tableName;
-(NSArray*)applicationsArray:(NSString*)tableName;
-(NSDictionary*)genresDictionary:(NSString*)tableName;
-(NSDictionary*)applicationDictionary:(NSString*)tableName;

@end
