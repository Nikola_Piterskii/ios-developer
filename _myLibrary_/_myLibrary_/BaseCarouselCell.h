//
//  BaseCarouselCell.h
//  _myLibrary_
//
//  Created by Алексей Иванков on 1/28/13.
//  Copyright (c) 2013 Алексей Иванков. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OpenViewDelegate.h"

@interface BaseCarouselCell : UIView

@property (retain,nonatomic) IBOutlet UIImageView *coverImage;
@property (retain,nonatomic) IBOutlet UIView *viewCell;
@property (nonatomic,assign) id<OpenViewDelegate> delegate;
@property (nonatomic,retain) IBOutlet UILabel *nameDirectory;
@property (nonatomic,retain) id _idView;




 @end 

