//
//  HomeScreeenViewController_ipad.m
//  _myLibrary_
//
//  Created by Алексей Иванков on 1/24/13.
//  Copyright (c) 2013 Алексей Иванков. All rights reserved.
//

#import "HomeScreeenViewController_ipad.h"
#import "CarouselCell_ipad.h"

@interface HomeScreeenViewController_ipad ()

@end

@implementation HomeScreeenViewController_ipad
-(BaseCarouselCell*)carouselCell
{
    if(_carouselCell==nil)
    {
        _carouselCell = [[CarouselCell_ipad alloc]init];
    }
    return _carouselCell;
}

@end
