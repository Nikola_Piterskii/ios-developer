//
//  FileDownloadRequest.h
//  _myLibrary_
//
//  Created by Алексей Иванков on 2/12/13.
//  Copyright (c) 2013 Алексей Иванков. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JsonObject.h"




@class FileDownloadRequest;

@protocol FileDownloadRequestDelegate <NSObject>
@optional
-(void) fileDownloadRequestSuccess:(FileDownloadRequest*)request jsonObject : (JsonObject*)jsonObject;
-(void) fileDownloadRequestFailed:(FileDownloadRequest*)request withError:(NSError*)error;
@end

@protocol ImageDownloadRequestDelegate <NSObject>
@optional
-(void) imageDownloadRequestSuccess : (NSString*)idImage;
@end

@interface FileDownloadRequest : NSObject

@property (nonatomic, retain) NSString* path;
@property (nonatomic, retain) NSString* url;
@property (nonatomic, assign) BOOL resumeDownload;
@property (nonatomic, assign) NSTimeInterval timeout;
@property (nonatomic, assign) id<FileDownloadRequestDelegate> delegate;
@property (nonatomic,assign) id<ImageDownloadRequestDelegate> imageDelegate;


-(void) start;
-(void) cancel;

@end
