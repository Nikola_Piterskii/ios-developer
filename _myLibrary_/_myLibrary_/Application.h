//
//  Application.h
//  _myLibrary_
//
//  Created by Николай Голдин on 22.02.13.
//  Copyright (c) 2013 Алексей Иванков. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Genre;

@interface Application : NSManagedObject

@property (nonatomic, retain) NSString * applicationName;
@property (nonatomic, retain) NSString * descriptionApplication;
@property (nonatomic, retain) NSNumber * id;
@property (nonatomic, retain) NSString * referencesItunes;
@property (nonatomic, retain) NSString * referencesImage;
@property (nonatomic, retain) Genre *genre;

@end
