//
//  categoryObject.h
//  _myLibrary_
//
//  Created by Алексей Иванков on 2/13/13.
//  Copyright (c) 2013 Алексей Иванков. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseCatalogCell.h"

@interface CategoryObject : NSObject

@property (retain,nonatomic) id _idCategory;
@property (retain, nonatomic) NSString *nameCategory;
@property (retain,nonatomic) BaseCatalogCell *catalogCell;

@end
