//
//  CatalogViewController.h
//  _myLibrary_
//
//  Created by Алексей Иванков on 1/24/13.
//  Copyright (c) 2013 Алексей Иванков. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"



/*
const NSInteger category_id[] = {
     6004,
     6005,
     6014,
     0  //обязательно массив должен заканчиваться 0 !
};
 */

@interface CategoryViewController : BaseViewController <UITableViewDelegate, UITableViewDataSource, NSFetchedResultsControllerDelegate>
@property (retain,nonatomic) IBOutlet UITableView *tableCatalog;
@property (retain, nonatomic) NSFetchedResultsController* fetchedResultController;


-(IBAction)onTouchHomeController:(id)sender;
@end
