//
//  FileDownloadRequest.m
//  _myLibrary_
//
//  Created by Алексей Иванков on 2/12/13.
//  Copyright (c) 2013 Алексей Иванков. All rights reserved.
//

#import "FileDownloadRequest.h"
#import "AFHTTPRequestOperation.h"
#import "AFImageRequestOperation.h"
#import "Utils.h"
#import "JsonObject.h"
#import "Constants.h"


@interface FileDownloadRequest ()
{
  
    NSOperationQueue *_queue;
}

@property (retain,nonatomic) AFHTTPRequestOperation *request;


-(void)sendJsonObject :(id)json;
-(void)loadImage: (NSString*)URL idImage:(NSString*)_id;

@end

@implementation FileDownloadRequest
@synthesize url;
@synthesize path;
@synthesize timeout;
@synthesize delegate;
@synthesize request;
@synthesize imageDelegate;

-(void)dealloc
{
    [url release];
    [path release];
    [request release];
    [super dealloc];
}

-(id)init
{
    self = [super init];
    if(self)
    {
        _queue =  [[NSOperationQueue alloc]init];
    }
    return self;
}


-(void)start
{
    [_queue addOperationWithBlock:^{

    NSMutableURLRequest* urlRequest = [[[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:self.url] cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:self.timeout] autorelease];
    self.request = [[[AFHTTPRequestOperation alloc] initWithRequest:urlRequest] autorelease];
    
    [self.request setCompletionBlockWithSuccess: ^(AFHTTPRequestOperation *operation, id responseObject)
     {
         NSError* error = nil;
         id json = [NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingMutableContainers error:&error];
         [self sendJsonObject:json];
     }
         failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         DLog(@"Download fail %@ -> %@. Error: %@", self.url, self.path, error);
         [self.delegate fileDownloadRequestFailed:self withError:error];
         
     }];
      [self.request start];
    }];
}


-(void)cancel
{
    if(self.request)
    {
        [self.request cancel];
    }
}

-(void)sendJsonObject:(id)json
{
    //NSLog(@"%@",json);
    NSDictionary *objectDictionary = [[json objectForKey:@"feed"] objectForKey:@"entry"];
    NSMutableArray * arrayPathImage = nil;
    JsonObject *jsonObject = nil;
    for(id object in objectDictionary)
    {
        jsonObject = [[[JsonObject alloc]init]autorelease];
        if([objectDictionary isKindOfClass:[NSDictionary class]])
        {
            object = objectDictionary;
        }
        jsonObject._id = [[[object objectForKey:@"id"]objectForKey:@"attributes"]objectForKey:@"im:id"];
        jsonObject._idCategory = [[[object objectForKey:@"category"]objectForKey:@"attributes"]objectForKey:@"im:id"];
        jsonObject.nameObject = [[object objectForKey:@"im:artist"]objectForKey:@"label"];
        NSDictionary* linkDictionary  = [ object objectForKey:@"link"];
        arrayPathImage = [NSMutableArray array];
        for (id link in linkDictionary) {
            [arrayPathImage addObject: [[link objectForKey:@"attributes"] objectForKey:@"href"]];
        }
        jsonObject.referenceObject = [NSString stringWithFormat:@"%@",[arrayPathImage objectAtIndex:0]];
        jsonObject.referenceBackgroundImageObject = [NSString stringWithFormat:@"%@",[arrayPathImage objectAtIndex:1]];
         const char *utf8string   = [[[object objectForKey:@"summary"] objectForKey:@"label"] UTF8String] ;
        jsonObject.descriptionObject = [NSString stringWithUTF8String: utf8string];
        [self loadImage:jsonObject.referenceBackgroundImageObject idImage:[NSString stringWithFormat:EXPANSION, jsonObject._id]];
        [self.delegate fileDownloadRequestSuccess:self jsonObject:jsonObject];
        if([objectDictionary isKindOfClass:[NSDictionary class]])
        {
            break;
        }    }
}

-(void)loadImage:(NSString *)URL idImage:(NSString *)_id
{
    [_queue addOperationWithBlock:^{
    
    NSString* stringURL = [NSString stringWithFormat:URL,1];
    NSURLRequest* urlRequest = [[[NSURLRequest alloc] initWithURL:[NSURL URLWithString:stringURL] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:10.0] autorelease];
    AFImageRequestOperation *imageRequest  = [[[AFImageRequestOperation alloc]initWithRequest:urlRequest]autorelease];
    NSString* filePath = [[Utils cacheDirectory] stringByAppendingPathComponent:_id];
    NSOutputStream *outputStream = [NSOutputStream outputStreamToFileAtPath:filePath append:NO];
    [imageRequest setOutputStream:outputStream];
    
    [imageRequest setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject)
    {
       [operation.outputStream close];
        DLog(@"Download success %@ -> %@", URL, _id);
        NSArray *split = [ _id componentsSeparatedByString:@"."];
        [self.imageDelegate imageDownloadRequestSuccess:[NSString stringWithFormat:@"%@",split[0]]];
    }
    failure:^(AFHTTPRequestOperation *operation, NSError *error)
    {
       DLog(@"Download image fail %@ -> %@. Error: %@", self.url, self.path, error);
    }];
        [imageRequest start];
    }];
}

@end
