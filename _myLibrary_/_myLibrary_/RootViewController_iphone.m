//
//  RootViewController_iphone.m
//  _myLibrary_
//
//  Created by Алексей Иванков on 1/24/13.
//  Copyright (c) 2013 Алексей Иванков. All rights reserved.
//

#import "RootViewController_iphone.h"
#import "SplashViewController_iphone.h"
#import "HomeScreeenViewController_iphone.h"
#import "InformationViewController_iphone.h"
#import "CategoryViewController_iphone.h"
#import "SelectViewController_iphone.h"



@interface RootViewController_iphone ()

@end

@implementation RootViewController_iphone

-(SplashViewController*)splashViewController
{
  if(_splashViewController==nil)
  {
      _splashViewController = [[SplashViewController_iphone alloc]initWithNibName:@"SplashViewController_iphone" bundle:nil];
  }
    return _splashViewController;
}

-(HomeScreeenViewController*)homeScreenViewController
{
   if(_homeScreenViewController==nil)
   {
       _homeScreenViewController = [[HomeScreeenViewController_iphone alloc]initWithNibName:@"HomeScreeenViewController_iphone" bundle:nil];
   }
    return _homeScreenViewController;
}

-(InformationViewController*)informationViewController
{
   if(_informationViewController==nil)
   {
       _informationViewController = [[InformationViewController_iphone alloc]initWithNibName:@"InformationViewController_iphone" bundle:nil];
   }
    return _informationViewController;
}

-(CategoryViewController*)catalogViewController
{
  if(_catalogViewController==nil)
  {
      _catalogViewController = [[CatalogViewController_iphone alloc]initWithNibName:@"CatalogViewController_iphone" bundle:nil];
  }
    return _catalogViewController;
}

-(SelectViewController*)selectViewController
{
    if(_selectViewController==nil)
    {
        _selectViewController = [[SelectViewController_iphone alloc]initWithNibName:@"SelectViewController_iphone" bundle:nil];
    }
    return _selectViewController;
}


@end
