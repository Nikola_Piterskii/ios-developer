//
//  InformationViewController.h
//  _myLibrary_
//
//  Created by Алексей Иванков on 1/24/13.
//  Copyright (c) 2013 Алексей Иванков. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "JsonObjectDelegate.h"

@interface InformationViewController : BaseViewController 
-(IBAction)onTouchHomeController:(id)sender;

@end
