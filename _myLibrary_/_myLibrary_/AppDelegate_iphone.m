//
//  AppDelegate_iphone.m
//  _myLibrary_
//
//  Created by Алексей Иванков on 1/24/13.
//  Copyright (c) 2013 Алексей Иванков. All rights reserved.
//

#import "AppDelegate_iphone.h"
#import "RootViewController_iphone.h"

@implementation AppDelegate_iphone
-(RootViewController*)rootViewController
{
    if(_rootViewController==nil)
    {
        _rootViewController = [[RootViewController_iphone alloc]initWithNibName:nil bundle:nil];
    }return _rootViewController;
}
@end
