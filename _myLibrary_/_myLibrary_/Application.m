//
//  Application.m
//  _myLibrary_
//
//  Created by Николай Голдин on 22.02.13.
//  Copyright (c) 2013 Алексей Иванков. All rights reserved.
//

#import "Application.h"
#import "Genre.h"


@implementation Application

@dynamic applicationName;
@dynamic descriptionApplication;
@dynamic id;
@dynamic referencesItunes;
@dynamic referencesImage;
@dynamic genre;

@end
