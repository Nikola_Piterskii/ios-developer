//
//  ChangeViewControllerDelegate.h
//  _myLibrary_
//
//  Created by Алексей Иванков on 1/24/13.
//  Copyright (c) 2013 Алексей Иванков. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ChangeViewControllerDelegate <NSObject>
-(void)nextViewControllerName: (NSString*)nameViewController animation : (UIViewAnimationOptions)animationOptions;
@end
