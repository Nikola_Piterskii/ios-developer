//
//  StroreKit.h
//  _myLibrary_
//
//  Created by Алексей Иванков on 2/20/13.
//  Copyright (c) 2013 Алексей Иванков. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <StoreKit/StoreKit.h>

@class Product;
@interface StroreKitHandler : NSObject<SKProductsRequestDelegate,SKPaymentTransactionObserver>

-(void)requestProduct;
-(void)orderProduct:(Product*)product;
-(void)restore;

+(StroreKitHandler*)getInstance;


@end
