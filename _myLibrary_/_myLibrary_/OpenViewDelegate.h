//
//  OpenViewDelegate.h
//  _myLibrary_
//
//  Created by Алексей Иванков on 1/29/13.
//  Copyright (c) 2013 Алексей Иванков. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol OpenViewDelegate <NSObject>
-(void)touchView :(UIView*)sender;
@end
