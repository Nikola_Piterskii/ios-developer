//
//  CaruselInformationController_ipad.m
//  _myLibrary_
//
//  Created by Алексей Иванков on 1/29/13.
//  Copyright (c) 2013 Алексей Иванков. All rights reserved.
//

#import "SelectViewController_ipad.h"

@interface SelectViewController_ipad ()

@end

@implementation SelectViewController_ipad

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
