//
//  JsonObjectDelegate.h
//  _myLibrary_
//
//  Created by Алексей Иванков on 2/8/13.
//  Copyright (c) 2013 Алексей Иванков. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JsonObject.h"
#import "Application.h"

@protocol JsonObjectDelegate <NSObject>
@optional
-(void) jsonObjectDownload :(JsonObject*)jsonObject;
-(void) sendJsonObject : (JsonObject*)jsonObject;
-(void) sendApplicationObject:(Application*)application;
@end
