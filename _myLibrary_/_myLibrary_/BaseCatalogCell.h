//
//  BaseCatalogCell.h
//  _myLibrary_
//
//  Created by Алексей Иванков on 2/1/13.
//  Copyright (c) 2013 Алексей Иванков. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseCatalogCell : UITableViewCell

@property (nonatomic,retain) IBOutlet UILabel *nameCatalog;
@end
