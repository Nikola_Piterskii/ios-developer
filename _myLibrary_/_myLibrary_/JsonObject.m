//
//  JsonObject.m
//  _myLibrary_
//
//  Created by Алексей Иванков on 2/8/13.
//  Copyright (c) 2013 Алексей Иванков. All rights reserved.
//

#import "JsonObject.h"

@implementation JsonObject
@synthesize _id;
@synthesize _idCategory;
@synthesize nameObject;
@synthesize backgroundImageObject;
@synthesize referenceBackgroundImageObject;
@synthesize descriptionObject;
@synthesize referenceObject;
@synthesize viewObject;

-(void)dealloc
{
   [_id release];
   [_idCategory release];
   [referenceObject release];
   [nameObject release];
   [backgroundImageObject release];
   [referenceBackgroundImageObject release];
   [descriptionObject release];
   [viewObject release];
   [super dealloc];
}

@end
