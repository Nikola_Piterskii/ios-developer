//
//  AppDelegate.m
//  _myLibrary_
//
//  Created by Алексей Иванков on 1/24/13.
//  Copyright (c) 2013 Алексей Иванков. All rights reserved.
//

#import "AppDelegate_shared.h"
#import "RootViewController.h"
#import "DataStorage.h"

@interface AppDelegate_shared()

-(void)setGenre;

@end

@implementation AppDelegate_shared
@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;
@synthesize window=_window;
@synthesize rootViewController = _rootViewController;

- (void)dealloc
{
    [_window release];
    [_rootViewController release];
    [_managedObjectContext release];
    [_managedObjectModel release];
    [_persistentStoreCoordinator release];
    [super dealloc];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [DataStorage sharedDataStorage].managedObjectContext = self.managedObjectContext;
    
    [self setGenre];
    
    self.window.rootViewController = self.rootViewController;
    [self.window makeKeyAndVisible];
    return YES;
}

-(void)setGenre{
    [[DataStorage sharedDataStorage] addGenre:@"News" withId:News];
    [[DataStorage sharedDataStorage] addGenre:@"Games" withId:Games];
    [[DataStorage sharedDataStorage] addGenre:@"Sport" withId:Sport];
    [[DataStorage sharedDataStorage] addGenre:@"Books" withId:Books];
    [[DataStorage sharedDataStorage] addGenre:@"Kiosk" withId:Kiosk];
    [[DataStorage sharedDataStorage] addGenre:@"Other" withId:Other];
    [[DataStorage sharedDataStorage] addGenre:@"Music" withId:Music];
    [[DataStorage sharedDataStorage] addGenre:@"Travel" withId:Travel];
    [[DataStorage sharedDataStorage] addGenre:@"Finance" withId:Finance];
    [[DataStorage sharedDataStorage] addGenre:@"Medical" withId:Medical];
    [[DataStorage sharedDataStorage] addGenre:@"Weather" withId:Weather];
    [[DataStorage sharedDataStorage] addGenre:@"Catalogs" withId:Catalogs];
    [[DataStorage sharedDataStorage] addGenre:@"Business" withId:Business];
    [[DataStorage sharedDataStorage] addGenre:@"HandBook" withId:HandBook];
    [[DataStorage sharedDataStorage] addGenre:@"Utilities" withId:Utilities];
    [[DataStorage sharedDataStorage] addGenre:@"Education" withId:Education];
    [[DataStorage sharedDataStorage] addGenre:@"WayOfLife" withId:WayOfLife];
    [[DataStorage sharedDataStorage] addGenre:@"Perfomance" withId:Perfomance];
    [[DataStorage sharedDataStorage] addGenre:@"Navigation" withId:Navigation];
    [[DataStorage sharedDataStorage] addGenre:@"FoodandDrink" withId:FoodandDrink];
    [[DataStorage sharedDataStorage] addGenre:@"Entertainment" withId:Entertainment];
    [[DataStorage sharedDataStorage] addGenre:@"SocialNetwork" withId:SocialNetwork];
    [[DataStorage sharedDataStorage] addGenre:@"PhotoandVideo" withId:PhotoandVideo];
    [[DataStorage sharedDataStorage] addGenre:@"HealthandFitness" withId:HealthandFitness];
}

- (void)applicationWillResignActive:(UIApplication *)application
{
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    [[DataStorage sharedDataStorage] saveContext];
}

#pragma mark - Core Data stack

- (NSManagedObjectContext *)managedObjectContext
{
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] init];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return _managedObjectContext;
}

- (NSManagedObjectModel *)managedObjectModel
{
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"_myLibrary_" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"_myLibrary_.sqlite"];
    
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        DLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }    
    
    return _persistentStoreCoordinator;
}

#pragma mark - Application's Documents directory

- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}



@end
