//
//  JsonObject.h
//  _myLibrary_
//
//  Created by Алексей Иванков on 2/8/13.
//  Copyright (c) 2013 Алексей Иванков. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JsonObject : NSObject
@property (nonatomic, retain) id _id;
@property (nonatomic,retain) id _idCategory;
@property (nonatomic,retain) NSString *nameObject;
@property (nonatomic,retain) UIImage *backgroundImageObject;
@property (nonatomic,retain) NSString *referenceBackgroundImageObject;
@property (nonatomic,retain) NSString *referenceObject;
@property (nonatomic ,retain) NSString *descriptionObject;
@property (nonatomic,retain) UIView *viewObject;
@end
