//
//  HomeScreeenViewController_iphone.m
//  _myLibrary_
//
//  Created by Алексей Иванков on 1/24/13.
//  Copyright (c) 2013 Алексей Иванков. All rights reserved.
//

#import "HomeScreeenViewController_iphone.h"
#import "CarouselCell_iphone.h"

@interface HomeScreeenViewController_iphone ()

@end

@implementation HomeScreeenViewController_iphone

-(BaseCarouselCell*)carouselCell
{
  if(_carouselCell==nil)
  {
      _carouselCell = [[CarouselCell_iphone alloc]init];
  }
    return _carouselCell;
}

@end
