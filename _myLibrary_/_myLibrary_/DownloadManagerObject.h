//
//  DownloadManagerObject.h
//  _myLibrary_
//
//  Created by Алексей Иванков on 2/8/13.
//  Copyright (c) 2013 Алексей Иванков. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JsonObject.h"
#import "JsonObjectDelegate.h"
#import "FileDownloadRequest.h"


@interface DownloadManagerObject : NSObject <FileDownloadRequestDelegate>

@property (nonatomic,assign) id<JsonObjectDelegate> jsonObjectDelegate;
@property (retain, nonatomic) FileDownloadRequest *fileRequest;

+(DownloadManagerObject*)getInstance;
-(void)startLoadContent : (NSString*)URL countObject:(NSInteger)countObject;

@end
