//
//  AppDelegate.h
//  _myLibrary_
//
//  Created by Алексей Иванков on 1/24/13.
//  Copyright (c) 2013 Алексей Иванков. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RootViewController.h"

//@class RootViewController;
@interface AppDelegate_shared : UIResponder <UIApplicationDelegate>
{
    RootViewController *_rootViewController;
}

@property (strong, nonatomic) IBOutlet UIWindow *window;
@property (retain,nonatomic) RootViewController *rootViewController;
@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

- (NSURL *)applicationDocumentsDirectory;

@end
