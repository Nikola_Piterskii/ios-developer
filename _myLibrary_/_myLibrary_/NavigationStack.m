//
//  NavigationStack.m
//  _myLibrary_
//
//  Created by Алексей Иванков on 1/24/13.
//  Copyright (c) 2013 Алексей Иванков. All rights reserved.
//

#import "NavigationStack.h"

@interface NavigationStack ()
{
 
}
@property (retain,nonatomic) NSMutableDictionary *stackViewController;
@end

@implementation NavigationStack
@synthesize curentViewControllerStack;
@synthesize stackViewController;

-(void)dealloc
{
  [curentViewControllerStack release];
  [stackViewController release];
  [super dealloc];
}

-(NSMutableDictionary*)stackViewController
{
 if(stackViewController==nil)
 {
     stackViewController = [[NSMutableDictionary alloc]init];
 }
    return stackViewController;
}

-(void)addViewControllerStack:(UIViewController *)controller
{
    if(controller==nil)return;
    [self.stackViewController setObject:controller forKey:[NSString stringWithFormat:@"%@",[controller class]]];
    self.curentViewControllerStack = (BaseViewController*)controller;
}

-(void)changeViewController:(NSString *)nameViewController
{
    if(nameViewController.length==0||nameViewController==nil)return;
    BaseViewController *controller = [self.stackViewController valueForKey: nameViewController];
    if(controller!=nil)
    {
        self.curentViewControllerStack = controller;
    }
}
@end
