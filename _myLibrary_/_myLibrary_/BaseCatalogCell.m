//
//  BaseCatalogCell.m
//  _myLibrary_
//
//  Created by Алексей Иванков on 2/1/13.
//  Copyright (c) 2013 Алексей Иванков. All rights reserved.
//

#import "BaseCatalogCell.h"

@implementation BaseCatalogCell
@synthesize nameCatalog;

-(void)dealloc
{
    [nameCatalog release];
    [super dealloc];
}


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
