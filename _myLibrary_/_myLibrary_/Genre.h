//
//  Genre.h
//  _myLibrary_
//
//  Created by Николай Голдин on 22.02.13.
//  Copyright (c) 2013 Алексей Иванков. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Application;

@interface Genre : NSManagedObject

@property (nonatomic, retain) NSNumber * genreId;
@property (nonatomic, retain) NSString * genreName;
@property (nonatomic, retain) NSSet *application;
@end

@interface Genre (CoreDataGeneratedAccessors)

- (void)addApplicationObject:(Application *)value;
- (void)removeApplicationObject:(Application *)value;
- (void)addApplication:(NSSet *)values;
- (void)removeApplication:(NSSet *)values;
@end
