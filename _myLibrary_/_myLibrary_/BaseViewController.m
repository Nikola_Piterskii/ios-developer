//
//  BaseViewController.m
//  _myLibrary_
//
//  Created by Алексей Иванков on 1/24/13.
//  Copyright (c) 2013 Алексей Иванков. All rights reserved.
//

#import "BaseViewController.h"

@interface BaseViewController ()
{

}
@end

static NSString *_homeNameController = nil;
static  NSString *_informationNameController = nil;
static NSString *_catalogNameController = nil;
static NSString *_selectNameController = nil;

@implementation BaseViewController
@synthesize delegate;
@synthesize navigationbar;
@synthesize bottomBar;
@synthesize jsonDelegate;

-(void)dealloc
{
   [navigationbar release];
   [bottomBar release];
   [super dealloc];
}

+(void)setHomeControllerName :(NSString*)homeNameController
{
  if(homeNameController)
  {
      _homeNameController = homeNameController;
      [_homeNameController retain];
  }
}

+(void)setInformationControllerName :(NSString*)informationNameController
{
    if(informationNameController)
    {
        _informationNameController = informationNameController;
        [_informationNameController retain];
    }
}

+(void)setCatalogControllerName :(NSString*) catalogNameController
{
    if(catalogNameController )
    {
        _catalogNameController = catalogNameController;
        [_catalogNameController retain];
    }
}

+(void)setSelectControllerName:(NSString *)informationCaruselNameController
{
   if(informationCaruselNameController)
   {
       _selectNameController = informationCaruselNameController;
       [_selectNameController retain];
   }
}

+(NSString*)getHomeName
{
    return _homeNameController;
}

+(NSString*)getCatalogName
{
   return _catalogNameController;
}

+(NSString*)getInformationName
{
   return _informationNameController;
}

+(NSString*)getSelectName
{
   return _selectNameController;
}

-(CGRect)getFrame
{
    CGFloat x = self.view.frame.origin.x;
    CGFloat y = self.view.frame.origin.y + self.navigationbar.frame.size.height;
    CGFloat height = self.view.frame.size.height - self.navigationbar.frame.size.height - self.bottomBar.frame.size.height;
    CGFloat width = self.view.frame.size.width;
    return  CGRectMake(x, y, width, height);
}

-(void)setFrame
{

}


@end
