//
//  CaruselInformationViewController.h
//  _myLibrary_
//
//  Created by Алексей Иванков on 1/29/13.
//  Copyright (c) 2013 Алексей Иванков. All rights reserved.
//

#import "BaseViewController.h"




@interface SelectViewController : BaseViewController <JsonObjectDelegate>

@property (retain, nonatomic) IBOutlet UIImageView *imageObject;
@property (retain, nonatomic) IBOutlet UITextView *summaryObject;
@property (retain,nonatomic)  NSString *curentLink;
@property (retain,nonatomic) IBOutlet UIView *baseView;


@property (retain, nonatomic) IBOutlet UIImageView *imageObject_p;
@property (retain, nonatomic) IBOutlet UITextView *summaryObject_p;
@property (retain,nonatomic)  NSString *curentLink_p;
@property (retain,nonatomic) IBOutlet UIView *baseView_p;

@property (retain, nonatomic) IBOutlet UIImageView *imageObject_l;
@property (retain, nonatomic) IBOutlet UITextView *summaryObject_l;
@property (retain,nonatomic)  NSString *curentLink_l;
@property (retain,nonatomic) IBOutlet UIView *baseView_l;

@property (retain, nonatomic) IBOutlet UIView *mountView;

-(IBAction)onTouchHomeController:(id)sender;
-(IBAction)onTouchCurentLink :(id)sender;

@end
