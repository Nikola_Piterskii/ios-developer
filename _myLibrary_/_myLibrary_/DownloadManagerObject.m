//
//  DownloadManagerObject.m
//  _myLibrary_
//
//  Created by Алексей Иванков on 2/8/13.
//  Copyright (c) 2013 Алексей Иванков. All rights reserved.
//

#import "DownloadManagerObject.h"
#import "JsonObject.h"
#import "Constants.h"
#import "AFImageRequestOperation.h"

@interface DownloadManagerObject()
{

}
@property (nonatomic,retain)  NSMutableData *dateObject;
@property (retain, nonatomic) AFImageRequestOperation* imageRequest;


@end

@implementation DownloadManagerObject
@synthesize dateObject;
@synthesize jsonObjectDelegate;
@synthesize imageRequest;
@synthesize fileRequest;

-(void)dealloc
{
   [dateObject release];
   [imageRequest release];
   [super dealloc];
}


-(NSMutableData*)dateObject
{
   if(dateObject==nil)
   {dateObject = [[NSMutableData alloc]init];}
    return dateObject;
}

-(FileDownloadRequest*)fileRequest
{
    if(fileRequest==nil)
    {
        fileRequest = [[FileDownloadRequest alloc]init];
        fileRequest.delegate = [DownloadManagerObject getInstance];
    }
    return fileRequest;
}

static DownloadManagerObject *_downloadManagar;

+(DownloadManagerObject*)getInstance
{
  if(_downloadManagar==nil)
  {
      _downloadManagar = [[DownloadManagerObject alloc]init];
  }
    return _downloadManagar;
}

#pragma mark - DownloadManagerObject Methods

-(void)startLoadContent:(NSString *)URL countObject:(NSInteger)countObject
{
    NSString* stringURL = [NSString stringWithFormat:URL,countObject];
    self.fileRequest.url = stringURL;
    self.fileRequest.timeout = 50;
    [self.fileRequest start];
}

#pragma mark - FileDownloadRequestDelegate

-(void) fileDownloadRequestSuccess:(FileDownloadRequest*)request jsonObject:(JsonObject *)jsonObject
{ 
    [self.jsonObjectDelegate jsonObjectDownload:jsonObject];
}

-(void) fileDownloadRequestFailed:(FileDownloadRequest*)request withError:(NSError*)error
{

}
@end
